package DAO;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.Alertes;
import model.Estadistiques_Municipis;

public class AlertesDAO extends GenericDAO<Alertes, Integer> implements AlertesDAOInterface {
	
	public void setAlertes(String contingut, Date data, String tipo_alerta, int codiEsta) {
		
		Estadistiques_MunicipisDAOInterface estaDAO = new Estadistiques_MunicipisDAO();
		Estadistiques_Municipis esta = estaDAO.get(codiEsta);
		if (esta != null) {
			Alertes alertes = new Alertes(contingut, data, tipo_alerta);
			alertes.setId_taula_estadistica(esta);
			esta.getAlertes().add(alertes);
			esta.setAlerta_creada(true);
			esta.setRealizada(true);
			Session session = sessionFactory.getCurrentSession();
			try {
				session.beginTransaction();
				session.saveOrUpdate(alertes);		
				session.getTransaction().commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();
			}
		} else {
			System.out.println("La estadistica " + codiEsta + " no existeix");
		}
	}
	
	public List<Alertes> listar() {
		Session session = sessionFactory.getCurrentSession();
		
		List <Alertes> alertes;
		
		try {
			session.beginTransaction();
			alertes = this.list();
			session.getTransaction().commit();
		
			return alertes;
		}catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
		return null;
		
	}

}
