package DAO;

import java.util.Date;
import java.util.List;

import model.Alertes;

public interface AlertesDAOInterface extends GenericDAOInterface<Alertes, Integer>{
	
	void setAlertes(String contingut, Date data, String tipo_alerta, int codiEsta);
	
	List<Alertes> listar();
	
	

}
