package DAO;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.COVID_Centres_docents;
import model.Centres_Docents;
import model.Usuari;

public class COVID_Centres_docentsDAO extends GenericDAO<COVID_Centres_docents, Integer> implements COVID_Centres_docentsDAOInterface{

	public void setDadaCovid(int codiCentre, String estat, Date dataRegistre, int alumnesConfin,
			int docentsConfin, int altresConfin, int alumnesPositiuAcum, int docentsPositiuAcum,
			int altresPositiuAcum) {
		
		COVID_Centres_docents covid = new COVID_Centres_docents(estat, dataRegistre, alumnesConfin,
				docentsConfin, altresConfin, alumnesPositiuAcum, docentsPositiuAcum, altresPositiuAcum);
		Centres_docentsDAO centreDAO = new Centres_docentsDAO();
		Centres_Docents centre = centreDAO.get(codiCentre);
		
		if (centre != null) {
		
			covid.setCodi_de_centre(centre);
			centre.getCovid_centres().add(covid);
			Session session = sessionFactory.getCurrentSession();
			try {
				session.beginTransaction();
				session.saveOrUpdate(covid);	
				session.saveOrUpdate(centre);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();
			}
		} else {
			System.out.println("No existeix el centre " + codiCentre);
		}
	}
	
	public List<COVID_Centres_docents> listar() {
		//MailDAO mDAO = new MailDAO();
		//List <Mail> correos = mDAO.listar(); 
		Session session = sessionFactory.getCurrentSession();
		
		List <COVID_Centres_docents> covid;
		
		try {
			session.beginTransaction();
			covid = this.list();
			session.getTransaction().commit();
		
			return covid;
		}catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
		return null;
		
	}

}
