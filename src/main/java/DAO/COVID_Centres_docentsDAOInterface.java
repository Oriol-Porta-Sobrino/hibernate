package DAO;

import java.util.Date;
import java.util.List;

import model.COVID_Centres_docents;

public interface COVID_Centres_docentsDAOInterface extends GenericDAOInterface<COVID_Centres_docents, Integer> {

	void setDadaCovid(int codiCentre, String estat, 
			Date dataRegistre, int alumnesConfin, int docentsConfin, 
			int altresConfin, int alumnesPositiuAcum, int docentsPositiuAcum, int altresPositiuAcum);
	
	List<COVID_Centres_docents> listar();
	
}
