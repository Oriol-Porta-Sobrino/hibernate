package DAO;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.COVID_Centres_docents;
import model.Centres_Docents;
import model.Municipis;
import model.Usuari;

public class Centres_docentsDAO extends GenericDAO<Centres_Docents, Integer> implements Centres_docentsDAOInterface {
	
	public List<Centres_Docents> listar() {

		Session session = sessionFactory.getCurrentSession();
		
		List <Centres_Docents> centres;
		
		try {
			session.beginTransaction();
			centres = this.list();
			session.getTransaction().commit();
		
			return centres;
		}catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
		return null;
		
	}

}
