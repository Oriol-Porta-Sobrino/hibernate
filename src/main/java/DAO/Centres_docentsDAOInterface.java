package DAO;

import java.util.List;

import model.Centres_Docents;

public interface Centres_docentsDAOInterface extends GenericDAOInterface<Centres_Docents, Integer> {
	
	List<Centres_Docents> listar();

}
