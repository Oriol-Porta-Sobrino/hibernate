package DAO;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.Estadistiques_Municipis;
import model.Municipis;
import model.Usuari;

public class Estadistiques_MunicipisDAO extends GenericDAO<Estadistiques_Municipis, Integer> implements Estadistiques_MunicipisDAOInterface {

	//Este metodo nos ha dado muchos problemas y funciona bien salvo que si comprobamos el mismo dato
	// que ya existe 2 veces nos da un error de conexion
	public void novaPeticio(int codiUsuari, int codiMunicipi, Date data) {
		boolean cen = true;
		boolean prueba = true;

		MunicipisDAO muniDAO = new MunicipisDAO();
		UsuariDAO usuariDAO = new UsuariDAO();
		
		Usuari usuari = usuariDAO.get(codiUsuari);
		Municipis muni = muniDAO.get(codiMunicipi);
		
		if (usuari == null || muni == null)
			prueba = false;	

		if (prueba) {
			List<Estadistiques_Municipis> lista = listar();
		
			Estadistiques_Municipis esta = null;
			Estadistiques_Municipis pro = null;
	
			
			boolean exist = false;
			Iterator<Estadistiques_Municipis> ite = lista.iterator();
			while (ite.hasNext() && exist == false) {
				pro = ite.next();
				if (pro.getCodi_municipi().getCodi_municipi() == muni.getCodi_municipi() && pro.getData_generacio().equals(data)) {
					exist = true;
					esta = pro;
				}
			}
			
			if (!exist) {
				esta = new Estadistiques_Municipis();
				esta.setData_generacio(data);
				esta.getUsuaris().add(usuari);
				esta.setCodi_municipi(muni);
				esta.setRealizada(false);
				usuari.getEstadistiques().add(esta);
				muni.getEstadistiques().add(esta);			
			} else {
				boolean repe = false;
				Iterator<Usuari> iteu = esta.getUsuaris().iterator();
				Usuari usu = new Usuari();
				while (iteu.hasNext() && exist == false) {
					usu = iteu.next();
					if (usu.getId() == usuari.getId()) {
						repe = true;
					}
				}
				if (!repe) {
					usuari.getEstadistiques().add(esta);
					esta.getUsuaris().add(usuari);
				} else {
					cen = false;
				}
			}
			if (cen) {
					this.saveOrUpdate(esta);
			}
		} else {
			if (usuari == null)
				System.out.println("L'usuari " + codiUsuari + " no existeix");
			else
				System.out.println("El municipi " + codiMunicipi + " no existeix");
		}
	
	}
	
	public List<Estadistiques_Municipis> listar() {

		Session session = sessionFactory.getCurrentSession();
		
		List <Estadistiques_Municipis> estadisticas;
		
		try {
			session.beginTransaction();
			estadisticas = this.list();
			session.getTransaction().commit();
		
			return estadisticas;
		}catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
		return null;
		
	}

}
