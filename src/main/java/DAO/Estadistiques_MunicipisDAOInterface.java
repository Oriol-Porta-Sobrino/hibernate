package DAO;

import java.util.Date;
import java.util.List;

import model.Estadistiques_Municipis;

public interface Estadistiques_MunicipisDAOInterface extends GenericDAOInterface<Estadistiques_Municipis, Integer> { 
	
	void novaPeticio(int codiUsuari, int codiMunicipi, Date data);
	
	List<Estadistiques_Municipis> listar();

}
