package DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.Municipis;
import model.Usuari;

public class MunicipisDAO extends GenericDAO<Municipis, Integer> implements MunicipisDAOInterface {

	public void setMunicipi(int codiMunicipi, String nomMunicipi, int codiComarca, String nomComarca, int Poblacio) {
		Session session = sessionFactory.getCurrentSession();
		Municipis municipi = new Municipis(codiMunicipi, nomMunicipi, codiComarca, nomComarca, Poblacio);
		try {
			session.beginTransaction();
			session.saveOrUpdate(municipi);		
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}
	
	public List<Municipis> listar() {
		//MailDAO mDAO = new MailDAO();
		//List <Mail> correos = mDAO.listar(); 
		Session session = sessionFactory.getCurrentSession();
		
		List <Municipis> muni;
		
		try {
			session.beginTransaction();
			muni = this.list();
			session.getTransaction().commit();
		
			return muni;
		}catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
		return null;
		
	}

}
