package DAO;

import java.util.List;

import model.Municipis;

public interface MunicipisDAOInterface extends GenericDAOInterface<Municipis, Integer> {
	
	void setMunicipi(int codiMunicipi, String nomMunicipi, int codiComarca, String nomComarca, int Poblacio);
	
	List<Municipis> listar();

}
