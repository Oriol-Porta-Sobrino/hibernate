package DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.Estadistiques_Municipis;
import model.Usuari;

public class UsuariDAO extends GenericDAO<Usuari, Integer> implements UsuariDAOInterface {

	public boolean registre(String nom, String cognom1, String cognom2) {
		Session session = sessionFactory.getCurrentSession();
		Usuari usuari = new Usuari(nom, cognom1, cognom2);
		try {
			session.beginTransaction();
			session.saveOrUpdate(usuari);		
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}
	
	public List<Usuari> listar() {
		//MailDAO mDAO = new MailDAO();
		//List <Mail> correos = mDAO.listar(); 
		Session session = sessionFactory.getCurrentSession();
		
		List <Usuari> usuaris;
		
		try {
			session.beginTransaction();
			usuaris = this.list();
			session.getTransaction().commit();
		
			return usuaris;
		}catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
		return null;
		
	}

}
