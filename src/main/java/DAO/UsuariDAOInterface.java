package DAO;

import java.util.List;

import model.Usuari;

public interface UsuariDAOInterface extends GenericDAOInterface<Usuari, Integer> {
	
	boolean registre(String nom, String cognom1, String cognom2); 
	
	List<Usuari> listar();

}
