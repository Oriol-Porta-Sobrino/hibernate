package main;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import DAO.AlertesDAO;
import DAO.AlertesDAOInterface;
import DAO.COVID_Centres_docentsDAO;
import DAO.COVID_Centres_docentsDAOInterface;
import DAO.Centres_docentsDAO;
import DAO.Centres_docentsDAOInterface;
import DAO.Estadistiques_MunicipisDAO;
import DAO.Estadistiques_MunicipisDAOInterface;
import DAO.MunicipisDAO;
import DAO.MunicipisDAOInterface;
import DAO.UsuariDAO;
import DAO.UsuariDAOInterface;
import model.COVID_Centres_docents;
import model.Centres_Docents;
import model.Estadistiques_Municipis;
import model.Municipis;
import model.Usuari;

public class MainDAO {
	
	private static Escaner esc;
	private AlertesDAOInterface alertaDAO;
	private Centres_docentsDAOInterface centresDAO;
	private COVID_Centres_docentsDAOInterface covidDAO;
	private Estadistiques_MunicipisDAOInterface estadistiquesDAO;
	private MunicipisDAOInterface municipisDAO;
	private UsuariDAOInterface usuariDAO;
	
	public static void main(String[] args) {
		
		esc = new Escaner();	
		MainDAO main = new MainDAO();
		main.iniciarDAO();
		main.menu();
		
		
	}
	
	private void iniciarDAO() {
		alertaDAO = new AlertesDAO();
		centresDAO = new Centres_docentsDAO();
		covidDAO = new COVID_Centres_docentsDAO();
		estadistiquesDAO = new Estadistiques_MunicipisDAO();
		municipisDAO = new MunicipisDAO();
		usuariDAO = new UsuariDAO();
	}
	
	private void menu() {
		boolean salir = false;
		boolean bd_full = false;
		while (!salir) {
			mostrarMenu();
			int numMenu;
			numMenu = esc.numero("opci�: ", 0, 6);
			switch (numMenu) {
			case 1:
				llenarDatos();
				bd_full=true;
				break;
			case 2:
				registraUsuari();
				break;
			case 3:
				setMunicipi();
				break;
			case 4:
				setDadaCovid();
				break;
			case 5:
				novaPeticio();
				break;
			case 6:
				if (bd_full) {
					Usuari usu = escogerUsuario();
					Map<Integer, int[]> no = new HashMap<Integer, int[]>();
					Map<Integer, Integer> llista_inci_acumulada = inci_acumulada();
					Map<Integer, Integer> risc = risc_Rebrot(llista_inci_acumulada, no);
					Map<Integer, Integer> velo = velocitat_Propagacio();
					
					System.out.println("Incidencia acumulada: " + llista_inci_acumulada);
					System.out.println("Risc de rebrot: " + risc);
					System.out.println("Velocitat de propagaci�: " + velo);
					//generarEstadistiques(no, llista_inci_acumulada, risc, velo);
					//generar_alertes();
				} else {
					System.out.println("Primer has d'omplir la bd");
				}
				break;
			case 0:
				salir = true;
				break;

			default:
				break;
			}
		}
	}
	
	private void generarEstadistiques(Map<Integer, int[]> no, Map<Integer, Integer> llista_inci_acumulada, 
			Map<Integer, Integer> risc, Map<Integer, Integer> velo) {
		Iterator<Municipis> municipis = municipisDAO.listar().iterator();
		Calendar c = Calendar.getInstance();
        Date data = c.getTime();
		while (municipis.hasNext()) {
			Municipis muni = municipis.next();
		Estadistiques_Municipis esta = new Estadistiques_Municipis(llista_inci_acumulada.get(muni.getCodi_municipi()) , 
				data, false, false, risc.get(muni.getCodi_municipi()), velo.get(muni.getCodi_municipi()), 600, 
				50, no.get(muni.getCodi_municipi())[1], no.get(muni.getCodi_municipi())[2], 
				no.get(muni.getCodi_municipi())[3], no.get(muni.getCodi_municipi())[4], no.get(muni.getCodi_municipi())[5], 
				false);
		}
	}
	
	//Metodo que genera las alertas si es necesario
	private void generar_alertes() {
        Iterator<Estadistiques_Municipis> ite = estadistiquesDAO.listar().iterator();
        Estadistiques_Municipis estadistiques;
        AlertesDAO a = new AlertesDAO();
        int contador=0;
        while (ite.hasNext()) {
            estadistiques = ite.next();
            if (estadistiques.getIncidencia_acumulada() >= 10) {
                a.setAlertes("S'ha superat el llindar d'incidencia acumulada: "+ String.valueOf(estadistiques.getIncidencia_acumulada()), estadistiques.getData_generacio(), "Incicendia Acumulada", estadistiques.getId());
                contador++;
            }

            if (estadistiques.getRisc_rebrot() > 50) {
                a.setAlertes("S'ha superat el llindar de Risc de rebrot: "+ String.valueOf(estadistiques.getRisc_rebrot()), estadistiques.getData_generacio(), "Risc de rebrot", estadistiques.getId());
                contador++;
            }

            if (estadistiques.getVelocitat_propagacio() > 1) {
                a.setAlertes("S'ha superat el llindar de Velocitat de propagaci�: "+ String.valueOf(estadistiques.getVelocitat_propagacio()), estadistiques.getData_generacio(), "Velocitat de propagaci�", estadistiques.getId());
                contador++;
            }

            if (contador == 3) {
            	a.setAlertes("Alerta maxima, s'han de tancar els centres" , estadistiques.getData_generacio(), "Les 3 alertes s'han activat", estadistiques.getId());
            	System.out.println("Els centres del municipi " + estadistiques.getCodi_municipi() + " han de tancar");
            }

        }
    }
	
	//Te permite escoger un usuario a la hora de hacer la simulacion
	private Usuari escogerUsuario() {
		Usuari usu = new Usuari();
		List<Usuari> lista = usuariDAO.listar();
		int sel;
		int num = 0;
		for (Usuari ite : lista) {
			System.out.println("Usuari " + num + " : " + ite.getNom());
			num++;
		}
		sel = esc.numero("numero usuari que vols", 0, lista.size()-1);
		usu = lista.get(sel);
		return usu;
	}
	
	//Muestra el menu
	private void mostrarMenu() {
		System.out.println("1: Poblar BD");
		System.out.println("2: Registre Usuari");
		System.out.println("3: Registre Municipi");
		System.out.println("4: Registre COVID a un centre educatiu");
		System.out.println("5: Peticio de generar estadistiques");
		System.out.println("6: Simulacio");
		System.out.println("0: Sortir");
	}
	
	//Calcula la velocidad de propagacion
	private Map<Integer, Integer> velocitat_Propagacio() {
		Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		Iterator<Municipis> ite = municipisDAO.listar().iterator();
		Municipis municipi;
		int velo = 0;
		int n = 0;
		int n1 = 0;
		int n2 = 0;
		int n5 = 0;
		int n6 = 0;
		int n7 = 0;
		while (ite.hasNext()) {
			municipi = ite.next();
			Set<Centres_Docents> centres = municipi.getCentres();			
			Iterator<Centres_Docents> centre = centres.iterator();
			while (centre.hasNext()) {
				Set<COVID_Centres_docents> covids = centre.next().getCovid_centres();
				Iterator<COVID_Centres_docents> covid = covids.iterator();
				Date data = maxData(covids);
				Calendar c = Calendar.getInstance();
				c.setTime(data);
				c.add(Calendar.DATE, -1);
		        Date day1 = c.getTime();
		        c.add(Calendar.DATE, -1);
		        Date day2 = c.getTime();
		        c.add(Calendar.DATE, -3);
		        Date day5 = c.getTime();
		        c.add(Calendar.DATE, -1);
		        Date day6 = c.getTime();
		        c.add(Calendar.DATE, -1);
		        Date day7 = c.getTime();
				while (covid.hasNext()) {
					COVID_Centres_docents cov = covid.next();
					if(cov.getData_del_registre().equals(data)){
						n += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day1)) {
						n1 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day2)) {
						n2 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day5)) {
						n5 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day6)) {
						n6 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day7)) {
						n7 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					}
				}
			}
			velo = n + n1 + n2 / n5 + n6 + n7;
			mapa.put(municipi.getCodi_municipi(), velo);
		}	
		return mapa;
	}
	
	//Calcula el riesgo de rebrote
	private Map<Integer, Integer> risc_Rebrot(Map<Integer, Integer> llista, Map<Integer, int[]> no) {
		Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		Iterator<Municipis> ite = municipisDAO.listar().iterator();
		Municipis municipi;
		int incidencia;
		int risc = 0;
		int n = 0;
		int n1 = 0;
		int n2 = 0;
		int n5 = 0;
		int n6 = 0;
		int n7 = 0;	
		int p7;
		while (ite.hasNext()) {			
			municipi = ite.next();				
			Set<Centres_Docents> centres = municipi.getCentres();							
			Iterator<Centres_Docents> centre = centres.iterator();
			while (centre.hasNext()) {
				Set<COVID_Centres_docents> covids = centre.next().getCovid_centres();
				Iterator<COVID_Centres_docents> covid = covids.iterator();
				Date data = maxData(covids);
				Calendar c = Calendar.getInstance();
				c.setTime(data);
				c.add(Calendar.DATE, -1);
		        Date day1 = c.getTime();
		        c.add(Calendar.DATE, -1);
		        Date day2 = c.getTime();
		        c.add(Calendar.DATE, -3);
		        Date day5 = c.getTime();
		        c.add(Calendar.DATE, -1);
		        Date day6 = c.getTime();
		        c.add(Calendar.DATE, -1);
		        Date day7 = c.getTime();
				while (covid.hasNext()) {
					COVID_Centres_docents cov = covid.next();
					if(cov.getData_del_registre().equals(data)){
						n += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day1)) {
						n1 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day2)) {
						n2 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day5)) {
						n5 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day6)) {
						n6 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					} else if (cov.getData_del_registre().equals(day7)) {
						n7 += cov.getAltres_positiu_acum() + cov.getDocents_positiu_acum() + cov.getAltres_positiu_acum();
					}	
				}
			}
			int[] ari = {n, n1, n2, n5, n6, n7};
			no.put(municipi.getCodi_municipi(), ari);
			p7 = (n + n1 + n2) / (n5 + n6 + n7);
			incidencia = llista.get(municipi.getCodi_municipi());
			risc = p7*incidencia;
			mapa.put(municipi.getCodi_municipi(), risc);
		}		
		return mapa;
	}
	
	//Sirve para introducir una fecha y pasarla como parametro, controla los dias respecto el mes y ano
	private Date introduceAno() {
		int topDia = 31;
		int any = esc.numero("any", 2019, 2020);
		int mes = esc.numero("mes", 1, 12);
		if (mes % 2 != 0)
			topDia = 31;
		else if (mes == 2)
			if (any == 2020)
				topDia = 28;
			else
				topDia = 27;
		else
			topDia = 30;
		any -= 1900;
		mes -= 1;
		int dia = esc.numero("dia", 1, topDia);
		Date dataRegistre = new Date(any, mes, dia);
		return dataRegistre;
	}
	
	//Calcula la fecha mas reciente de los covid + centres docents seleccionados para saber de que fecha partir
	private Date maxData(Set<COVID_Centres_docents> covids) {
		Iterator<COVID_Centres_docents> ite = covids.iterator();
		COVID_Centres_docents covid;
		Date data = new Date(100,0,1);
		while (ite.hasNext()) {
			covid = ite.next();
			if (covid.getData_del_registre().after(data))
				data = covid.getData_del_registre();
		}
		return data;
	}
	
	//Calcula la incidencia de un municipio en concreto para poder calcular el riesgo de rebrote
	private int inci(Municipis muni) {
		int inci_acu_m = 0;
		if (muni != null) {
			Municipis m = muni;
			Set<Centres_Docents> centres = m.getCentres();
			COVID_Centres_docents cov_centre;
			int alumnes=0;
			int docent=0;
			int altres=0;
			int total_covid14=0;		
			for (Centres_Docents c : centres) {
				Set<COVID_Centres_docents>a=c.getCovid_centres();
				Iterator<COVID_Centres_docents> ite=a.iterator();
				cov_centre=ite.next();
				alumnes += cov_centre.getAlumnes_positiu_vigl4();
				docent += cov_centre.getDocents_positiu_vigl4();
				altres += cov_centre.getAltres_positius_vigl4();	
			}
			
			total_covid14=(alumnes+docent+altres);
			inci_acu_m=(total_covid14*100000)/m.getPoblacio();	
		}
		return inci_acu_m;
	}
	
	//Calcula la incidencia acumulada de todos los municipios
	private Map<Integer, Integer> inci_acumulada() {
		int inci_acu_m=0;
		Map<Integer, Integer> llista_inci_acumulada = new HashMap<Integer, Integer>();
		Iterator<Municipis> muni = municipisDAO.listar().iterator();	
		while (muni.hasNext()) {
			Municipis m = muni.next();
			Set<Centres_Docents> centres = m.getCentres();
			COVID_Centres_docents cov_centre;
			int alumnes=0;
			int docent=0;
			int altres=0;
			int total_covid14=0;		
			for (Centres_Docents c : centres) {
				Set<COVID_Centres_docents>a=c.getCovid_centres();
				Iterator<COVID_Centres_docents> ite=a.iterator();
				cov_centre=ite.next();
				alumnes += cov_centre.getAlumnes_positiu_vigl4();
				docent += cov_centre.getDocents_positiu_vigl4();
				altres += cov_centre.getAltres_positius_vigl4();	
			}
			
			total_covid14=(alumnes+docent+altres);
			inci_acu_m=(total_covid14*100000)/m.getPoblacio();	
			llista_inci_acumulada.put(m.getCodi_municipi(), inci_acu_m);
		}
		return llista_inci_acumulada;
		
	}
	
	//Registra un usuario nuevo
	private void registraUsuari() {
		String nom = esc.text("nom");
		String cognom1 = esc.text("primer cognom");
		String cognom2 = esc.text("segon cognom");
		usuariDAO.registre(nom, cognom1, cognom2);
	}
	
	//Registra un municipio nuevo
	private void setMunicipi() {
		int codiMunicipi = esc.numero("codi municipi", 0, 99999);
		String nomMunicipi = esc.text("nom municipi");
		int codiComarca = esc.numero("codi comarca", 0, 999);
		String nomComarca = esc.text("nom comarca");
		int poblacio = esc.numero("poblacio", 0, 1000000);
		municipisDAO.setMunicipi(codiMunicipi, nomMunicipi, codiComarca, nomComarca, poblacio);
	}
	
	//Registra un COVID + centres educatius nuevo
	private void setDadaCovid() {
		int codiCentre = esc.numero("codi centre", 0, 9999999);
		String estat = "";
		boolean cen = false;
		while (!cen) {
			estat = esc.text("estat del centre 'Obert/Tancat'");
			if (estat.equalsIgnoreCase("Obert") || estat.equalsIgnoreCase("Tancat"))
				cen = true;
		}
		Date dataRegistre = introduceAno();
		int alumnesConfin = esc.numero("alumnes confinats", 0, 9999);
		int docentsConfin = esc.numero("docents confinats", 0, 999);
		int altresConfin = esc.numero("altres confinats", 0, 9999);
		int alumnesPositiuAcum = esc.numero("alumnes positius confinats acumulats", 0, 9999);
		int docentsPositiuAcum = esc.numero("docents positius confinats acumulats", 0, 999);
		int altresPositiuAcum = esc.numero("altres positius confinats acumulats", 0, 9999);
		covidDAO.setDadaCovid(codiCentre, estat, dataRegistre, alumnesConfin, docentsConfin, altresConfin, alumnesPositiuAcum, docentsPositiuAcum, altresPositiuAcum);
	}
	
	//Registra una estadistica de municipio si no existe, si existe a�ade al usuario a la lista de usuarios
	// si ese usuario ya esta en la lista no hace nada
	private void novaPeticio() {
		int codiUsuari = esc.numero("codi de usuari", 0, 99999);
		int codiMunicipi = esc.numero("codi municipi", 0, 99999);
		Date data = introduceAno();
		estadistiquesDAO.novaPeticio(codiUsuari, codiMunicipi, data);
	}
	
	//Se crean los datos y las relaciones
	private void llenarDatos() {
		
		Calendar c = Calendar.getInstance();
        Date data = c.getTime();
        c.add(Calendar.DATE, -1);
        Date n1 = c.getTime();
        c.add(Calendar.DATE, -1);
        Date n2 = c.getTime();
        c.add(Calendar.DATE, -3);
        Date n5 = c.getTime();
        c.add(Calendar.DATE, -1);
        Date n6 = c.getTime();
        c.add(Calendar.DATE, -1);
        Date n7 = c.getTime();
		
		Municipis m1 = new Municipis(81878, "Sabadell", 40, "Vall�s Occidental", 213644); // 4, 22, 45, 52, 19
		Municipis m2 = new Municipis(81672, "Poliny�", 40, "Vall�s Occidental", 8479); // 0, 0, 2, 1, 0
		Municipis m3 = new Municipis(81568, "Palau-solit� i Plegamans", 40, "Vall�s Occidental", 14771); // 0, 4, 2, 2, 0
		Municipis m4 = new Municipis(82606, "Santa Perp�tua de Mogoda", 40, "Vall�s Occidental", 25799); // 4, 1, 5, 7, 0
		Municipis m5 = new Municipis(80517, "Castellar del Vall�s", 40, "Vall�s Occidental", 24187); // 0, 1, 0, 4, 0
		Municipis m6 = new Municipis(82520, "Barber� del Vall�s", 40, "Vall�s Occidental", 33091); // 1, 7, 5, 6, 5
		Municipis m7 = new Municipis(82798, "Terrassa", 40, "Vall�s Occidental", 220556); // 7, 32, 26, 34, 8
		Municipis m8 = new Municipis(82671, "Sentmenat", 40, "Vall�s Occidental", 9078); // 0, 0, 0, 0, 0, 1
		Municipis m9 = new Municipis(89045, "Badia del Vall�s", 40, "Vall�s Occidental", 13380); // 0, 2, 5, 3, 0
		Municipis m10 = new Municipis(82665, "Cerdanyola del Vall�s", 40, "Vall�s Occidental", 57403); // 1, 2, 5, 8, 3
		
		Usuari u1 = new Usuari("Isaac", "Garc�a", "Jim�nez");
		Usuari u2 = new Usuari("Oriol", "Porta", "Sobrino");
		Usuari u3 = new Usuari("Eloi", "Vazquez", "Batista");
		
		Centres_Docents c1_m1 = new Centres_Docents(8044624, "Institut Sabadell", "c. Juvenal, 1", "08206"); 
		Centres_Docents c2_m1 = new Centres_Docents(8024741, "Institut Escola Industrial", "c. Calder�n, 56", "08201");
		Centres_Docents c3_m1 = new Centres_Docents(8046669, "Institut Ribot i Serra", "c. Concha Espina, 33", "08204");
		Centres_Docents c4_m1 = new Centres_Docents(8024546, "Sant Francesc", "c. Pizarro, 18", "08204");
		Centres_Docents c5_m1 = new Centres_Docents(8055105, "Escola Arraona", "c. Argentina, 45", "08205");			
		
		Centres_Docents c1_m2 = new Centres_Docents(8053170, "Institut Poliny�","av. Sabadell, 1-3", "08213");
		Centres_Docents c2_m2 = new Centres_Docents(8022872, "Escola Pere Calders", "pg. Sanllehy, s/n", "08213");
		
		Centres_Docents c1_m3 = new Centres_Docents(8045306, "Institut Ramon Casas i Carb�", "c. Lluis Companys, 2", "08184");
		Centres_Docents c2_m3 = new Centres_Docents(8022616, "Escola Josep Maria Folch i Torres", "c. Folch i Torras, 30", "08184");
		Centres_Docents c3_m3 = new Centres_Docents(8053467, "Escola Palau", "c. Arquitecte Falguera, 37", "08184");
		
		Centres_Docents c1_m4 = new Centres_Docents(8045021, "Institut Estela Ib�rica", "c. Passatge de mas Granollacs, s/n", "08130");
		Centres_Docents c2_m4 = new Centres_Docents(8034461, "Escola Bernat de Mogoda", "c. Pau Picasso, 2", "08130");
		Centres_Docents c3_m4 = new Centres_Docents(8053236, "Institut Rovira-Forns", "c. Tierno Galvan, 77", "08130");
		Centres_Docents c4_m4 = new Centres_Docents(8028485, "Sagrada Fam�lia", "c. Puig i Cadafalch, s/n", "08130");
		Centres_Docents c5_m4 = new Centres_Docents(8028497, "Escola Santa Perp�tua", "c. de Tierno Galv�n, 79", "08130");
		
		Centres_Docents c1_m5 = new Centres_Docents(8076558, "Institut Escola Sant Esteve", "c. Prat de la Riba, s/n", "08211");
		Centres_Docents c2_m5 = new Centres_Docents(8046682, "Institut de Castellar", "c. Carrasco i Formiguera, 6", "08211");
		Centres_Docents c3_m5 = new Centres_Docents(8054824, "Escola Mestre Pla", "c. Josep Carner, 2", "08211");	
		
		Centres_Docents c1_m6 = new Centres_Docents(8076546, "Institut Escola Can Llobet", "ptge. Dr. Moragas, 243", "08210");
		Centres_Docents c2_m6 = new Centres_Docents(8035349, "Institut La Rom�nica", "rda. de Santa Maria, 310", "08210");
		Centres_Docents c3_m6 = new Centres_Docents(8039641, "Escola Pablo Picasso", "c. Can�ries, 26", "08210");

		Centres_Docents c1_m7 = new Centres_Docents(8076601, "Institut Escola Pere Viver", "C. Arenys de Mar, 2", "08221");
		Centres_Docents c2_m7 = new Centres_Docents(8034059, "Institut Nicolau Cop�rnic", "c. del Torrent del Batlle, 10", "08225");
		Centres_Docents c3_m7 = new Centres_Docents(8030078, "Egara", "ctra. de Castellar, 126", "08222");
		Centres_Docents c4_m7 = new Centres_Docents(8024777, "Institut Egara", "c. Am�rica, 55 (Can Parellada)", "08228");
		Centres_Docents c5_m7 = new Centres_Docents(8031770, "Institut Can Jofresa", "av. Can Jofresa, 9", "08223");
		
		Centres_Docents c1_m8 = new Centres_Docents(8053248, "Institut de Sentmenat", "c. Poca Farina, s/n", "08181");
		Centres_Docents c2_m8 = new Centres_Docents(8028990, "Escola Can Sorts", "c. Joanot Martorell, s/n", "08181");

		Centres_Docents c1_m9 = new Centres_Docents(8042342, "Institut de Badia del Vall�s", "c. Mallorca, s/n", "08214");
		Centres_Docents c2_m9 = new Centres_Docents(8034035, "Institut Federica Montseny", "c. Oporto, s/n", "08214");			

		Centres_Docents c1_m10 = new Centres_Docents(8043504, "Institut Pere Calders", "Campus U.A.B.", "08193");
		Centres_Docents c2_m10 = new Centres_Docents(8045549, "Institut Ban�s", "c. Sant Casimir, 16", "08290");	
		Centres_Docents c3_m10 = new Centres_Docents(8028849, "Escaladei", "c. Sant Salvador, 8", "08290");
		Centres_Docents c4_m10 = new Centres_Docents(8031757, "Institut Forat del Vent", "Pizarro, 35", "08290");
		Centres_Docents c5_m10 = new Centres_Docents(8028886, "Montserrat", "c. Sant Iscle, 6", "08290");
		
		COVID_Centres_docents covid_c1_m1 = new COVID_Centres_docents("Obert", data, 3, 43, 3, 0, 0, 29, 1, 0, 3, 2, 0, 0);
		COVID_Centres_docents covid_c2_m1 = new COVID_Centres_docents("Obert", data, 1, 19, 1, 0, 0, 37, 1, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c3_m1 = new COVID_Centres_docents("Obert", data, 3, 48, 1, 0, 0, 36, 0, 0, 3, 3, 0, 0);
		COVID_Centres_docents covid_c4_m1 = new COVID_Centres_docents("Obert", data, 2, 43, 2, 0, 0, 7, 2, 0, 2, 3, 0, 0);
		COVID_Centres_docents covid_c5_m1 = new COVID_Centres_docents("Obert", data, 2, 35, 2, 0, 0, 8, 1, 0, 2, 3, 0, 0);
		
		COVID_Centres_docents covid_c1_m2 = new COVID_Centres_docents("Obert", data , 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m2 = new COVID_Centres_docents("Obert", data , 2, 34, 3, 0, 0, 4, 0, 1, 2, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m3 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m3 = new COVID_Centres_docents("Obert", data, 1, 17, 1, 0, 0, 3, 0, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c3_m3 = new COVID_Centres_docents("Obert", data, 2, 33, 4, 0, 0, 5, 2, 0, 2, 2, 0, 0);
		
		COVID_Centres_docents covid_c1_m4 = new COVID_Centres_docents("Obert", data, 1, 20, 1, 0, 0, 27, 2, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c2_m4 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m4 = new COVID_Centres_docents("Obert", data, 1, 10, 1, 0, 0, 22, 2, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c4_m4 = new COVID_Centres_docents("Obert", data, 1, 40, 1, 0, 0, 12, 2, 2, 1, 0, 0, 0);
		COVID_Centres_docents covid_c5_m4 = new COVID_Centres_docents("Obert", data, 1, 17, 1, 0, 0, 3, 1, 0, 1, 1, 0, 0);
		
		COVID_Centres_docents covid_c1_m5 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 6, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m5 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 23, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m5 = new COVID_Centres_docents("Obert", data, 0, 1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m6 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 9, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m6 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 34, 3, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c3_m6 = new COVID_Centres_docents("Obert", data, 2, 35, 3, 0, 0, 17, 1, 0, 2, 5, 1, 0);
		
		COVID_Centres_docents covid_c1_m7 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 15, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m7 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 14, 3, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m7 = new COVID_Centres_docents("Obert", data, 3, 85, 2, 0, 0, 41, 9, 0, 3, 10, 3, 0);
		COVID_Centres_docents covid_c4_m7 = new COVID_Centres_docents("Obert", data, 2, 46, 1, 0, 0, 19, 3, 0, 2, 1, 1, 0);
		COVID_Centres_docents covid_c5_m7 = new COVID_Centres_docents("Obert", data, 1, 24, 1, 0, 0, 30, 1, 0, 1, 2, 0, 0);
		
		COVID_Centres_docents covid_c1_m8 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m8 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 11, 1, 0, 0, 1, 0, 0);
		
		COVID_Centres_docents covid_c1_m9 = new COVID_Centres_docents("Obert", data, 1, 20, 0, 0, 0, 14, 4, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c2_m9 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m10 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m10 = new COVID_Centres_docents("Obert", data, 1, 24, 1, 0, 0, 16, 2, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c3_m10 = new COVID_Centres_docents("Obert", data, 2, 40, 4, 0, 0, 7, 0, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c4_m10 = new COVID_Centres_docents("Obert", data, 0, 29, 0, 0, 0, 12, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c5_m10 = new COVID_Centres_docents("Obert", data, 2, 52, 1, 0, 0, 6, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m1_1 = new COVID_Centres_docents("Obert", n1, 3, 43, 3, 0, 0, 29, 1, 0, 3, 2, 0, 0);
		COVID_Centres_docents covid_c1_m1_2 = new COVID_Centres_docents("Obert", n2, 3, 43, 3, 0, 0, 29, 1, 0, 3, 2, 0, 0);
		COVID_Centres_docents covid_c1_m1_5 = new COVID_Centres_docents("Obert", n5, 2, 30, 1, 0, 0, 25, 1, 0, 2, 1, 0, 0);
		COVID_Centres_docents covid_c1_m1_6 = new COVID_Centres_docents("Obert", n6, 2, 25, 1, 0, 0, 25, 1, 0, 2, 1, 0, 0);
		COVID_Centres_docents covid_c1_m1_7 = new COVID_Centres_docents("Obert", n7, 2, 25, 1, 0, 0, 25, 1, 0, 2, 1, 0, 0);
		
		COVID_Centres_docents covid_c2_m1_1 = new COVID_Centres_docents("Obert", n1, 1,	19, 1,	0, 0, 37, 1, 0, 1, 3, 0,	0);		
		COVID_Centres_docents covid_c2_m1_2 = new COVID_Centres_docents("Obert", n2, 1, 19,	1, 	0, 0, 34, 1, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c2_m1_5 = new COVID_Centres_docents("Obert", n5, 1, 19, 1, 	0, 0, 34, 1, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c2_m1_6 = new COVID_Centres_docents("Obert", n6, 1, 15, 1, 	0, 0, 34, 1, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c2_m1_7 = new COVID_Centres_docents("Obert", n7, 1, 15, 1, 	0, 0, 34, 1, 0, 1, 2, 0, 0);	
		
		COVID_Centres_docents covid_c3_m1_1 = new COVID_Centres_docents("Obert", n1, 3, 48, 1, 0, 0, 36, 0, 0, 3, 3, 0, 0);
		COVID_Centres_docents covid_c3_m1_2 = new COVID_Centres_docents("Obert", n2, 3, 48, 1, 0, 0, 36, 0, 0, 3, 3, 0, 0);
		COVID_Centres_docents covid_c3_m1_5 = new COVID_Centres_docents("Obert", n5, 2 , 30, 0, 0, 0, 20, 0, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m1_6 = new COVID_Centres_docents("Obert", n6, 1, 20, 0, 0, 0, 15, 0, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c3_m1_7 = new COVID_Centres_docents("Obert", n7, 1, 20, 0, 0, 0, 15, 0, 0, 1, 1, 0, 0);
		
		COVID_Centres_docents covid_c4_m1_1 = new COVID_Centres_docents("Obert", n1, 2, 43, 2, 0, 0, 7, 2, 0, 2, 3, 0,	0);
		COVID_Centres_docents covid_c4_m1_2 = new COVID_Centres_docents("Obert", n2, 2, 43, 2, 0, 0, 6, 1, 0, 2, 3, 0,	0);
		COVID_Centres_docents covid_c4_m1_5 = new COVID_Centres_docents("Obert", n5, 1, 23, 1, 0, 0, 4, 1, 0, 1, 2, 0,	0);
		COVID_Centres_docents covid_c4_m1_6 = new COVID_Centres_docents("Obert", n6, 1, 23, 1, 0, 0, 3, 1, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c4_m1_7 = new COVID_Centres_docents("Obert", n7, 1, 23, 1, 0, 0, 3, 1, 0, 1, 2, 0, 0);	
		
		COVID_Centres_docents covid_c5_m1_1 = new COVID_Centres_docents("Obert", n1, 2, 35, 2, 0, 0, 8, 1, 0, 2, 3, 0, 0);
		COVID_Centres_docents covid_c5_m1_2 = new COVID_Centres_docents("Obert", n2, 2, 28, 2, 0, 0, 8, 1, 0, 2, 3, 0, 0);
		COVID_Centres_docents covid_c5_m1_5 = new COVID_Centres_docents("Obert", n5, 1, 18, 1, 0, 0, 8, 1, 0, 1, 2, 0, 0);	
		COVID_Centres_docents covid_c5_m1_6 = new COVID_Centres_docents("Obert", n6, 1, 15, 1, 0, 0, 7, 1, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c5_m1_7 = new COVID_Centres_docents("Obert", n7, 1, 15, 1, 0, 0, 7, 1, 0, 1, 1, 0, 0);
		
		COVID_Centres_docents covid_c1_m2_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m2_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m2_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m2_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m2_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m2_1 = new COVID_Centres_docents("Obert", n1, 2, 33, 3, 0, 0, 4, 0, 1, 2, 0, 0, 0);
		COVID_Centres_docents covid_c2_m2_2 = new COVID_Centres_docents("Obert", n2, 2, 30, 3, 0, 0, 4, 0, 1, 2, 0, 0, 0);
		COVID_Centres_docents covid_c2_m2_5 = new COVID_Centres_docents("Obert", n5, 2, 30, 3, 0, 0, 2, 0, 1, 2, 0, 0, 0);
		COVID_Centres_docents covid_c2_m2_6 = new COVID_Centres_docents("Obert", n6, 2, 30, 3, 0, 0, 2, 0, 1, 2, 0, 0, 0);
		COVID_Centres_docents covid_c2_m2_7 = new COVID_Centres_docents("Obert", n7, 2, 28, 1, 0, 0, 2, 0, 1, 1, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m3_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m3_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m3_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m3_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m3_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m3_1 = new COVID_Centres_docents("Obert", n1, 1, 17, 1, 0, 0, 3, 0, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c2_m3_2 = new COVID_Centres_docents("Obert", n2, 1, 17, 1, 0, 0, 3, 0, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c2_m3_5 = new COVID_Centres_docents("Obert", n5, 1, 15, 1, 0, 0, 3, 0, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c2_m3_6 = new COVID_Centres_docents("Obert", n6, 0, 15, 1, 0, 0, 1, 0, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c2_m3_7 = new COVID_Centres_docents("Obert", n7, 1, 11, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0);
		
		COVID_Centres_docents covid_c3_m3_1 = new COVID_Centres_docents("Obert", n1, 2, 33, 4, 0, 0, 5, 2, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m3_2 = new COVID_Centres_docents("Obert", n2, 2, 33, 4, 0, 0, 5, 2, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m3_5 = new COVID_Centres_docents("Obert", n5, 2, 30, 2, 0, 0, 4, 1, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m3_6 = new COVID_Centres_docents("Obert", n6, 2, 30, 1, 0, 0, 3, 2, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m3_7 = new COVID_Centres_docents("Obert", n7, 0, 27, 0, 0, 0, 2, 2, 0, 2, 2, 0, 0);
		
		COVID_Centres_docents covid_c1_m4_1 = new COVID_Centres_docents("Obert", n1, 1, 20, 1, 0, 0, 27, 2, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c1_m4_2 = new COVID_Centres_docents("Obert", n2, 1, 20, 1, 0, 0, 24, 2, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c1_m4_5 = new COVID_Centres_docents("Obert", n5, 1, 19, 1, 0, 0, 25, 2, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c1_m4_6 = new COVID_Centres_docents("Obert", n6, 1, 18, 1, 0, 0, 25, 2, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c1_m4_7 = new COVID_Centres_docents("Obert", n7, 1, 17, 1, 0, 0, 24, 2, 0, 1, 2, 0, 0);
		
		COVID_Centres_docents covid_c2_m4_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m4_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m4_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m4_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m4_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c3_m4_1 = new COVID_Centres_docents("Obert", n1, 1, 10, 1, 0, 0, 22, 2, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c3_m4_2 = new COVID_Centres_docents("Obert", n2, 1, 10, 1, 0, 0, 20, 2, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c3_m4_5 = new COVID_Centres_docents("Obert", n5, 1, 7, 1, 0, 0, 20, 2, 0, 1, 3, 0, 0);
		COVID_Centres_docents covid_c3_m4_6 = new COVID_Centres_docents("Obert", n6, 1, 4, 1, 0, 0, 21, 0, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m4_7 = new COVID_Centres_docents("Obert", n7, 1, 1, 1, 0, 0, 20, 2, 0, 1, 2, 0, 0);
		
		COVID_Centres_docents covid_c4_m4_1 = new COVID_Centres_docents("Obert", n1, 1, 40, 1, 0, 0, 12, 2, 2, 1, 0, 0, 0);
		COVID_Centres_docents covid_c4_m4_2 = new COVID_Centres_docents("Obert", n2, 1, 40, 1, 0, 0, 12, 2, 2, 1, 0, 0, 0);
		COVID_Centres_docents covid_c4_m4_5 = new COVID_Centres_docents("Obert", n5, 1, 40, 1, 0, 0, 12, 2, 2, 1, 0, 0, 0);
		COVID_Centres_docents covid_c4_m4_6 = new COVID_Centres_docents("Obert", n6, 1, 40, 1, 0, 0, 12, 2, 2, 1, 0, 0, 0);
		COVID_Centres_docents covid_c4_m4_7 = new COVID_Centres_docents("Obert", n7, 1, 40, 1, 0, 0, 12, 2, 2, 1, 0, 0, 0);
		
		COVID_Centres_docents covid_c5_m4_1 = new COVID_Centres_docents("Obert", n1, 1, 15, 1, 0, 0, 3, 1, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c5_m4_2 = new COVID_Centres_docents("Obert", n2, 1, 15, 1, 0, 0, 3, 1, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c5_m4_5 = new COVID_Centres_docents("Obert", n5, 0, 15, 1, 0, 0, 2, 1, 0, 2, 1, 0, 0);
		COVID_Centres_docents covid_c5_m4_6 = new COVID_Centres_docents("Obert", n6, 1, 14, 1, 0, 0, 2, 1, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c5_m4_7 = new COVID_Centres_docents("Obert", n7, 1, 13, 1, 0, 0, 1, 2, 0, 1, 3, 0, 0);
		
		COVID_Centres_docents covid_c1_m5_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 6, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m5_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m5_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m5_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m5_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m5_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 23, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m5_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 20, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m5_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m5_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 16, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m5_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c3_m5_1 = new COVID_Centres_docents("Obert", n1, 0, 1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m5_2 = new COVID_Centres_docents("Obert", n2, 0, 1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m5_5 = new COVID_Centres_docents("Obert", n5, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m5_6 = new COVID_Centres_docents("Obert", n6, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c3_m5_7 = new COVID_Centres_docents("Obert", n7, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m6_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 8, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m6_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 7, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m6_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m6_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m6_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m6_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 30, 3, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m6_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 30, 2, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m6_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 25, 2, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m6_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 20, 2, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m6_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 20, 1, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c3_m6_1 = new COVID_Centres_docents("Obert", n1, 2, 35, 3, 0, 0, 15, 1, 0, 2, 5, 1, 0);
		COVID_Centres_docents covid_c3_m6_2 = new COVID_Centres_docents("Obert", n2, 2, 35, 3, 0, 0, 14, 1, 0, 2, 4, 1, 0);
		COVID_Centres_docents covid_c3_m6_5 = new COVID_Centres_docents("Obert", n5, 2, 35, 3, 0, 0, 13, 0, 0, 2, 3, 1, 0);
		COVID_Centres_docents covid_c3_m6_6 = new COVID_Centres_docents("Obert", n6, 2, 35, 3, 0, 0, 11, 0, 0, 2, 2, 1, 0);
		COVID_Centres_docents covid_c3_m6_7 = new COVID_Centres_docents("Obert", n7, 2, 35, 3, 0, 0, 9, 0, 0, 2, 1, 0, 0);
		
		COVID_Centres_docents covid_c1_m7_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 13, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m7_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 10, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m7_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 8, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m7_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m7_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m7_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 12, 3, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m7_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 10, 3, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m7_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 8, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m7_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 6, 2, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m7_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c3_m7_1 = new COVID_Centres_docents("Obert", n1, 3, 85, 2, 0, 0, 40, 9, 0, 3, 10, 3, 0);
		COVID_Centres_docents covid_c3_m7_2 = new COVID_Centres_docents("Obert", n2, 3, 85, 2, 0, 0, 35, 7, 0, 3, 8, 2, 0);
		COVID_Centres_docents covid_c3_m7_5 = new COVID_Centres_docents("Obert", n5, 3, 85, 2, 0, 0, 32, 5, 0, 1, 8, 1, 0);
		COVID_Centres_docents covid_c3_m7_6 = new COVID_Centres_docents("Obert", n6, 3, 85, 2, 0, 0, 32, 5, 0, 1, 7, 0, 0);
		COVID_Centres_docents covid_c3_m7_7 = new COVID_Centres_docents("Obert", n7, 3, 85, 2, 0, 0, 30, 4, 0, 0, 5, 0, 0);
		
		COVID_Centres_docents covid_c4_m7_1 = new COVID_Centres_docents("Obert", n1, 2, 46, 1, 0, 0, 17, 3, 0, 2, 1, 1, 0);
		COVID_Centres_docents covid_c4_m7_2 = new COVID_Centres_docents("Obert", n2, 2, 46, 1, 0, 0, 17, 2, 0, 1, 1, 1, 0);
		COVID_Centres_docents covid_c4_m7_5 = new COVID_Centres_docents("Obert", n5, 2, 46, 1, 0, 0, 15, 2, 0, 1, 0, 0, 0);
		COVID_Centres_docents covid_c4_m7_6 = new COVID_Centres_docents("Obert", n6, 2, 46, 1, 0, 0, 13, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c4_m7_7 = new COVID_Centres_docents("Obert", n7, 2, 46, 1, 0, 0, 13, 0, 0, 0, 0, 1, 0);
		
		COVID_Centres_docents covid_c5_m7_1 = new COVID_Centres_docents("Obert", n1, 1, 24, 1, 0, 0, 30, 1, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c5_m7_2 = new COVID_Centres_docents("Obert", n2, 1, 24, 1, 0, 0, 27, 1, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c5_m7_5 = new COVID_Centres_docents("Obert", n5, 1, 24, 1, 0, 0, 25, 1, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c5_m7_6 = new COVID_Centres_docents("Obert", n6, 1, 24, 1, 0, 0, 20, 0, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c5_m7_7 = new COVID_Centres_docents("Obert", n7, 1, 24, 1, 0, 0, 20, 1, 0, 0, 1, 0, 0);
		
		COVID_Centres_docents covid_c1_m8_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m8_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m8_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m8_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m8_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m8_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 11, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m8_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 10, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m8_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 10, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c2_m8_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 9, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m8_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m9_1 = new COVID_Centres_docents("Obert", n1, 1, 20, 0, 0, 0, 14, 4, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c1_m9_2 = new COVID_Centres_docents("Obert", n2, 1, 20, 0, 0, 0, 10, 4, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c1_m9_5 = new COVID_Centres_docents("Obert", n5, 1, 20, 0, 0, 0, 10, 2, 0, 1, 0, 0, 0);
		COVID_Centres_docents covid_c1_m9_6 = new COVID_Centres_docents("Obert", n6, 1, 20, 0, 0, 0, 9, 2, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c1_m9_7 = new COVID_Centres_docents("Obert", n7, 1, 20, 0, 0, 0, 7, 0, 0, 0, 1, 0, 0);
		
		COVID_Centres_docents covid_c2_m9_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m9_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m9_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m9_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c2_m9_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c1_m10_1 = new COVID_Centres_docents("Obert", n1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m10_2 = new COVID_Centres_docents("Obert", n2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m10_5 = new COVID_Centres_docents("Obert", n5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m10_6 = new COVID_Centres_docents("Obert", n6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c1_m10_7 = new COVID_Centres_docents("Obert", n7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		
		COVID_Centres_docents covid_c2_m10_1 = new COVID_Centres_docents("Obert", n1, 1, 24, 1, 0, 0, 16, 2, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c2_m10_2 = new COVID_Centres_docents("Obert", n2, 1, 23, 1, 0, 0, 15, 2, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c2_m10_5 = new COVID_Centres_docents("Obert", n5, 1, 20, 1, 0, 0, 14, 2, 0, 1, 1, 0, 0);
		COVID_Centres_docents covid_c2_m10_6 = new COVID_Centres_docents("Obert", n6, 1, 18, 1, 0, 0, 13, 1, 0, 1, 0, 0, 0);
		COVID_Centres_docents covid_c2_m10_7 = new COVID_Centres_docents("Obert", n7, 0, 26, 1, 0, 0, 12, 0, 0, 0, 1, 0, 0);
		
		COVID_Centres_docents covid_c3_m10_1 = new COVID_Centres_docents("Obert", n1, 2, 40, 4, 0, 0, 7, 0, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m10_2 = new COVID_Centres_docents("Obert", n2, 1, 38, 3, 0, 0, 6, 0, 0, 2, 2, 0, 0);
		COVID_Centres_docents covid_c3_m10_5 = new COVID_Centres_docents("Obert", n5, 1, 36, 2, 0, 0, 5, 0, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c3_m10_6 = new COVID_Centres_docents("Obert", n6, 0, 34, 1, 0, 0, 4, 0, 0, 1, 2, 0, 0);
		COVID_Centres_docents covid_c3_m10_7 = new COVID_Centres_docents("Obert", n7, 0, 32, 0, 0, 0, 4, 0, 0, 1, 0, 0, 0);
		
		COVID_Centres_docents covid_c4_m10_1 = new COVID_Centres_docents("Obert", n1, 0, 29, 0, 0, 0, 12, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c4_m10_2 = new COVID_Centres_docents("Obert", n2, 0, 27, 0, 0, 0, 11, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c4_m10_5 = new COVID_Centres_docents("Obert", n5, 0, 25, 0, 0, 0, 11, 1, 0, 0, 1, 0, 0);
		COVID_Centres_docents covid_c4_m10_6 = new COVID_Centres_docents("Obert", n6, 0, 20, 0, 0, 0, 11, 1, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c4_m10_7 = new COVID_Centres_docents("Obert", n7, 0, 16, 0, 0, 0, 10, 1, 0, 0, 1, 0, 0);
		
		COVID_Centres_docents covid_c5_m10_1 = new COVID_Centres_docents("Obert", n1, 2, 50, 1, 0, 0, 6, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c5_m10_2 = new COVID_Centres_docents("Obert", n2, 1, 45, 1, 0, 0, 5, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c5_m10_5 = new COVID_Centres_docents("Obert", n5, 1, 40, 1, 0, 0, 6, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c5_m10_6 = new COVID_Centres_docents("Obert", n6, 0, 35, 1, 0, 0, 4, 0, 0, 0, 0, 0, 0);
		COVID_Centres_docents covid_c5_m10_7 = new COVID_Centres_docents("Obert", n7, 0, 30, 1, 0, 0, 4, 0, 0, 0, 0, 0, 0);		

		
		m1.getCentres().add(c1_m1);
		m1.getCentres().add(c2_m1);
		m1.getCentres().add(c3_m1);
		m1.getCentres().add(c4_m1);
		m1.getCentres().add(c5_m1);
		
		m2.getCentres().add(c1_m2);
		m2.getCentres().add(c2_m2);
		
		m3.getCentres().add(c1_m3);
		m3.getCentres().add(c2_m3);
		m3.getCentres().add(c3_m3);
		
		m4.getCentres().add(c1_m4);
		m4.getCentres().add(c2_m4);
		m4.getCentres().add(c3_m4);
		m4.getCentres().add(c4_m4);
		m4.getCentres().add(c5_m4);
		
		m5.getCentres().add(c1_m5);
		m5.getCentres().add(c2_m5);
		m5.getCentres().add(c3_m5);
		
		m6.getCentres().add(c1_m6);
		m6.getCentres().add(c2_m6);
		m6.getCentres().add(c3_m6);
		
		m7.getCentres().add(c1_m7);
		m7.getCentres().add(c2_m7);
		m7.getCentres().add(c3_m7);
		m7.getCentres().add(c4_m7);
		m7.getCentres().add(c5_m7);
		
		m8.getCentres().add(c1_m8);
		m8.getCentres().add(c2_m8);
		
		m9.getCentres().add(c1_m9);
		m9.getCentres().add(c2_m9);
		
		m10.getCentres().add(c1_m10);
		m10.getCentres().add(c2_m10);
		m10.getCentres().add(c3_m10);
		m10.getCentres().add(c4_m10);
		m10.getCentres().add(c5_m10);
		
		c1_m1.setCodi_municipi(m1);
		c2_m1.setCodi_municipi(m1);
		c3_m1.setCodi_municipi(m1);
		c4_m1.setCodi_municipi(m1);
		c5_m1.setCodi_municipi(m1);
		
		c1_m2.setCodi_municipi(m2);
		c2_m2.setCodi_municipi(m2);
		
		c1_m3.setCodi_municipi(m3);
		c2_m3.setCodi_municipi(m3);
		c3_m3.setCodi_municipi(m3);
		
		c1_m4.setCodi_municipi(m4);
		c2_m4.setCodi_municipi(m4);
		c3_m4.setCodi_municipi(m4);
		c4_m4.setCodi_municipi(m4);
		c5_m4.setCodi_municipi(m4);
		
		c1_m5.setCodi_municipi(m5);
		c2_m5.setCodi_municipi(m5);
		c3_m5.setCodi_municipi(m5);
		
		c1_m6.setCodi_municipi(m6);
		c2_m6.setCodi_municipi(m6);
		c3_m6.setCodi_municipi(m6);
		
		c1_m7.setCodi_municipi(m7);
		c2_m7.setCodi_municipi(m7);
		c3_m7.setCodi_municipi(m7);
		c4_m7.setCodi_municipi(m7);
		c5_m7.setCodi_municipi(m7);
		
		c1_m8.setCodi_municipi(m8);
		c2_m8.setCodi_municipi(m8);
		
		c1_m9.setCodi_municipi(m9);
		c2_m9.setCodi_municipi(m9);
		
		c1_m10.setCodi_municipi(m10);
		c2_m10.setCodi_municipi(m10);
		c3_m10.setCodi_municipi(m10);
		c4_m10.setCodi_municipi(m10);
		c5_m10.setCodi_municipi(m10);
		
		c1_m1.getCentres().add(c2_m1);
        c2_m1.getCentres().add(c1_m1);
        c1_m2.getCentres().add(c2_m2);
        c2_m2.getCentres().add(c1_m2);
        c1_m3.getCentres().add(c2_m3);
        c2_m3.getCentres().add(c1_m3);
        c1_m4.getCentres().add(c2_m4);
        c2_m4.getCentres().add(c1_m4);
        c1_m5.getCentres().add(c2_m5);
        c2_m5.getCentres().add(c1_m5);
        c1_m6.getCentres().add(c2_m6);
        c2_m6.getCentres().add(c1_m6);
        c1_m7.getCentres().add(c2_m7);
        c2_m7.getCentres().add(c1_m7);
        c1_m8.getCentres().add(c2_m8);
        c2_m8.getCentres().add(c1_m8);
        c1_m9.getCentres().add(c2_m9);
        c2_m9.getCentres().add(c1_m9);
        c1_m10.getCentres().add(c2_m10);
        c2_m10.getCentres().add(c1_m10);
		
		covid_c1_m1.setCodi_de_centre(c1_m1);
		covid_c2_m1.setCodi_de_centre(c2_m1);
		covid_c3_m1.setCodi_de_centre(c3_m1);
		covid_c4_m1.setCodi_de_centre(c4_m1);
		covid_c5_m1.setCodi_de_centre(c5_m1);
		
		covid_c1_m2.setCodi_de_centre(c1_m2);
		covid_c2_m2.setCodi_de_centre(c2_m2);
		
		covid_c1_m3.setCodi_de_centre(c1_m3);
		covid_c2_m3.setCodi_de_centre(c2_m3);
		covid_c3_m3.setCodi_de_centre(c3_m3);
		
		covid_c1_m4.setCodi_de_centre(c1_m4);
		covid_c2_m4.setCodi_de_centre(c2_m4);
		covid_c3_m4.setCodi_de_centre(c3_m4);
		covid_c4_m4.setCodi_de_centre(c4_m4);
		covid_c5_m4.setCodi_de_centre(c5_m4);

		covid_c1_m5.setCodi_de_centre(c1_m5);
		covid_c2_m5.setCodi_de_centre(c2_m5);
		covid_c3_m5.setCodi_de_centre(c3_m5);
		
		covid_c1_m6.setCodi_de_centre(c1_m6);
		covid_c2_m6.setCodi_de_centre(c2_m6);
		covid_c3_m6.setCodi_de_centre(c3_m6);
		
		covid_c1_m7.setCodi_de_centre(c1_m7);
		covid_c2_m7.setCodi_de_centre(c2_m7);
		covid_c3_m7.setCodi_de_centre(c3_m7);
		covid_c4_m7.setCodi_de_centre(c4_m7);
		covid_c5_m7.setCodi_de_centre(c5_m7);
		
		covid_c1_m8.setCodi_de_centre(c1_m8);
		covid_c2_m8.setCodi_de_centre(c2_m8);
		
		covid_c1_m9.setCodi_de_centre(c1_m9);
		covid_c2_m9.setCodi_de_centre(c2_m9);
		
		covid_c1_m10.setCodi_de_centre(c1_m10);
		covid_c2_m10.setCodi_de_centre(c2_m10);
		covid_c3_m10.setCodi_de_centre(c3_m10);
		covid_c4_m10.setCodi_de_centre(c4_m10);
		covid_c5_m10.setCodi_de_centre(c5_m10);
		
		covid_c1_m1_1.setCodi_de_centre(c1_m1);
		covid_c1_m1_2.setCodi_de_centre(c1_m1);
		covid_c1_m1_5.setCodi_de_centre(c1_m1);
		covid_c1_m1_6.setCodi_de_centre(c1_m1);
		covid_c1_m1_7.setCodi_de_centre(c1_m1);
		
		covid_c2_m1_1.setCodi_de_centre(c2_m1);
		covid_c2_m1_2.setCodi_de_centre(c2_m1);
		covid_c2_m1_5.setCodi_de_centre(c2_m1);
		covid_c2_m1_6.setCodi_de_centre(c2_m1);
		covid_c2_m1_7.setCodi_de_centre(c2_m1);
		
		covid_c3_m1_1.setCodi_de_centre(c3_m1);
		covid_c3_m1_2.setCodi_de_centre(c3_m1);
		covid_c3_m1_5.setCodi_de_centre(c3_m1);
		covid_c3_m1_6.setCodi_de_centre(c3_m1);
		covid_c3_m1_7.setCodi_de_centre(c3_m1);
		
		covid_c4_m1_1.setCodi_de_centre(c4_m1);
		covid_c4_m1_2.setCodi_de_centre(c4_m1);
		covid_c4_m1_5.setCodi_de_centre(c4_m1);
		covid_c4_m1_6.setCodi_de_centre(c4_m1);
		covid_c4_m1_7.setCodi_de_centre(c4_m1);
		
		covid_c5_m1_1.setCodi_de_centre(c5_m1);
		covid_c5_m1_2.setCodi_de_centre(c5_m1);
		covid_c5_m1_5.setCodi_de_centre(c5_m1);
		covid_c5_m1_6.setCodi_de_centre(c5_m1);
		covid_c5_m1_7.setCodi_de_centre(c5_m1);
		
		covid_c1_m2_1.setCodi_de_centre(c1_m2);
		covid_c1_m2_2.setCodi_de_centre(c1_m2);
		covid_c1_m2_5.setCodi_de_centre(c1_m2);
		covid_c1_m2_6.setCodi_de_centre(c1_m2);
		covid_c1_m2_7.setCodi_de_centre(c1_m2);
		
		covid_c2_m2_1.setCodi_de_centre(c2_m2);
		covid_c2_m2_2.setCodi_de_centre(c2_m2);
		covid_c2_m2_5.setCodi_de_centre(c2_m2);
		covid_c2_m2_6.setCodi_de_centre(c2_m2);
		covid_c2_m2_7.setCodi_de_centre(c2_m2);
		
		covid_c1_m3_1.setCodi_de_centre(c1_m3);
		covid_c1_m3_2.setCodi_de_centre(c1_m3);
		covid_c1_m3_5.setCodi_de_centre(c1_m3);
		covid_c1_m3_6.setCodi_de_centre(c1_m3);
		covid_c1_m3_7.setCodi_de_centre(c1_m3);
		
		covid_c2_m3_1.setCodi_de_centre(c2_m3);
		covid_c2_m3_2.setCodi_de_centre(c2_m3);
		covid_c2_m3_5.setCodi_de_centre(c2_m3);
		covid_c2_m3_6.setCodi_de_centre(c2_m3);
		covid_c2_m3_7.setCodi_de_centre(c2_m3);
		
		covid_c3_m3_1.setCodi_de_centre(c3_m3);
		covid_c3_m3_2.setCodi_de_centre(c3_m3);
		covid_c3_m3_5.setCodi_de_centre(c3_m3);
		covid_c3_m3_6.setCodi_de_centre(c3_m3);
		covid_c3_m3_7.setCodi_de_centre(c3_m3);
		
		covid_c1_m4_1.setCodi_de_centre(c1_m4);
		covid_c1_m4_2.setCodi_de_centre(c1_m4);
		covid_c1_m4_5.setCodi_de_centre(c1_m4);
		covid_c1_m4_6.setCodi_de_centre(c1_m4);
		covid_c1_m4_7.setCodi_de_centre(c1_m4);
		
		covid_c2_m4_1.setCodi_de_centre(c2_m4);
		covid_c2_m4_2.setCodi_de_centre(c2_m4);
		covid_c2_m4_5.setCodi_de_centre(c2_m4);
		covid_c2_m4_6.setCodi_de_centre(c2_m4);
		covid_c2_m4_7.setCodi_de_centre(c2_m4);
		
		covid_c3_m4_1.setCodi_de_centre(c3_m4);
		covid_c3_m4_2.setCodi_de_centre(c3_m4);
		covid_c3_m4_5.setCodi_de_centre(c3_m4);
		covid_c3_m4_6.setCodi_de_centre(c3_m4);
		covid_c3_m4_7.setCodi_de_centre(c3_m4);
		
		covid_c4_m4_1.setCodi_de_centre(c4_m4);
		covid_c4_m4_2.setCodi_de_centre(c4_m4);
		covid_c4_m4_5.setCodi_de_centre(c4_m4);
		covid_c4_m4_6.setCodi_de_centre(c4_m4);
		covid_c4_m4_7.setCodi_de_centre(c4_m4);
		
		covid_c5_m4_1.setCodi_de_centre(c5_m4);
		covid_c5_m4_2.setCodi_de_centre(c5_m4);
		covid_c5_m4_5.setCodi_de_centre(c5_m4);
		covid_c5_m4_6.setCodi_de_centre(c5_m4);
		covid_c5_m4_7.setCodi_de_centre(c5_m4);
		
		covid_c1_m5_1.setCodi_de_centre(c1_m5);
		covid_c1_m5_2.setCodi_de_centre(c1_m5);
		covid_c1_m5_5.setCodi_de_centre(c1_m5);
		covid_c1_m5_6.setCodi_de_centre(c1_m5);
		covid_c1_m5_7.setCodi_de_centre(c1_m5);
		
		covid_c2_m5_1.setCodi_de_centre(c2_m5);
		covid_c2_m5_2.setCodi_de_centre(c2_m5);
		covid_c2_m5_5.setCodi_de_centre(c2_m5);
		covid_c2_m5_6.setCodi_de_centre(c2_m5);
		covid_c2_m5_7.setCodi_de_centre(c2_m5);
		
		covid_c3_m5_1.setCodi_de_centre(c3_m5);
		covid_c3_m5_2.setCodi_de_centre(c3_m5);
		covid_c3_m5_5.setCodi_de_centre(c3_m5);
		covid_c3_m5_6.setCodi_de_centre(c3_m5);
		covid_c3_m5_7.setCodi_de_centre(c3_m5);
		
		covid_c1_m6_1.setCodi_de_centre(c1_m6);
		covid_c1_m6_2.setCodi_de_centre(c1_m6);
		covid_c1_m6_5.setCodi_de_centre(c1_m6);
		covid_c1_m6_6.setCodi_de_centre(c1_m6);
		covid_c1_m6_7.setCodi_de_centre(c1_m6);
		
		covid_c2_m6_1.setCodi_de_centre(c2_m6);
		covid_c2_m6_2.setCodi_de_centre(c2_m6);
		covid_c2_m6_5.setCodi_de_centre(c2_m6);
		covid_c2_m6_6.setCodi_de_centre(c2_m6);
		covid_c2_m6_7.setCodi_de_centre(c2_m6);
		
		covid_c3_m6_1.setCodi_de_centre(c3_m6);
		covid_c3_m6_2.setCodi_de_centre(c3_m6);
		covid_c3_m6_5.setCodi_de_centre(c3_m6);
		covid_c3_m6_6.setCodi_de_centre(c3_m6);
		covid_c3_m6_7.setCodi_de_centre(c3_m6);
		
		covid_c1_m7_1.setCodi_de_centre(c1_m7);
		covid_c1_m7_2.setCodi_de_centre(c1_m7);
		covid_c1_m7_5.setCodi_de_centre(c1_m7);
		covid_c1_m7_6.setCodi_de_centre(c1_m7);
		covid_c1_m7_7.setCodi_de_centre(c1_m7);
		
		covid_c2_m7_1.setCodi_de_centre(c2_m7);
		covid_c2_m7_2.setCodi_de_centre(c2_m7);
		covid_c2_m7_5.setCodi_de_centre(c2_m7);
		covid_c2_m7_6.setCodi_de_centre(c2_m7);
		covid_c2_m7_7.setCodi_de_centre(c2_m7);
		
		covid_c3_m7_1.setCodi_de_centre(c3_m7);
		covid_c3_m7_2.setCodi_de_centre(c3_m7);
		covid_c3_m7_5.setCodi_de_centre(c3_m7);
		covid_c3_m7_6.setCodi_de_centre(c3_m7);
		covid_c3_m7_7.setCodi_de_centre(c3_m7);
		
		covid_c4_m7_1.setCodi_de_centre(c4_m7);
		covid_c4_m7_2.setCodi_de_centre(c4_m7);
		covid_c4_m7_5.setCodi_de_centre(c4_m7);
		covid_c4_m7_6.setCodi_de_centre(c4_m7);
		covid_c4_m7_7.setCodi_de_centre(c4_m7);
		
		covid_c5_m7_1.setCodi_de_centre(c5_m7);
		covid_c5_m7_2.setCodi_de_centre(c5_m7);
		covid_c5_m7_5.setCodi_de_centre(c5_m7);
		covid_c5_m7_6.setCodi_de_centre(c5_m7);
		covid_c5_m7_7.setCodi_de_centre(c5_m7);
		
		covid_c1_m8_1.setCodi_de_centre(c1_m8);
		covid_c1_m8_2.setCodi_de_centre(c1_m8);
		covid_c1_m8_5.setCodi_de_centre(c1_m8);
		covid_c1_m8_6.setCodi_de_centre(c1_m8);
		covid_c1_m8_7.setCodi_de_centre(c1_m8);
		
		covid_c2_m8_1.setCodi_de_centre(c2_m8);
		covid_c2_m8_2.setCodi_de_centre(c2_m8);
		covid_c2_m8_5.setCodi_de_centre(c2_m8);
		covid_c2_m8_6.setCodi_de_centre(c2_m8);
		covid_c2_m8_7.setCodi_de_centre(c2_m8);
		
		covid_c1_m9_1.setCodi_de_centre(c1_m9);
		covid_c1_m9_2.setCodi_de_centre(c1_m9);
		covid_c1_m9_5.setCodi_de_centre(c1_m9);
		covid_c1_m9_6.setCodi_de_centre(c1_m9);
		covid_c1_m9_7.setCodi_de_centre(c1_m9);
		
		covid_c2_m9_1.setCodi_de_centre(c2_m9);
		covid_c2_m9_2.setCodi_de_centre(c2_m9);
		covid_c2_m9_5.setCodi_de_centre(c2_m9);
		covid_c2_m9_6.setCodi_de_centre(c2_m9);
		covid_c2_m9_7.setCodi_de_centre(c2_m9);
		
		covid_c1_m10_1.setCodi_de_centre(c1_m10);
		covid_c1_m10_2.setCodi_de_centre(c1_m10);
		covid_c1_m10_5.setCodi_de_centre(c1_m10);
		covid_c1_m10_6.setCodi_de_centre(c1_m10);
		covid_c1_m10_7.setCodi_de_centre(c1_m10);
		
		covid_c2_m10_1.setCodi_de_centre(c2_m10);
		covid_c2_m10_2.setCodi_de_centre(c2_m10);
		covid_c2_m10_5.setCodi_de_centre(c2_m10);
		covid_c2_m10_6.setCodi_de_centre(c2_m10);
		covid_c2_m10_7.setCodi_de_centre(c2_m10);
		
		covid_c3_m10_1.setCodi_de_centre(c3_m10);
		covid_c3_m10_2.setCodi_de_centre(c3_m10);
		covid_c3_m10_5.setCodi_de_centre(c3_m10);
		covid_c3_m10_6.setCodi_de_centre(c3_m10);
		covid_c3_m10_7.setCodi_de_centre(c3_m10);
		
		covid_c4_m10_1.setCodi_de_centre(c4_m10);
		covid_c4_m10_2.setCodi_de_centre(c4_m10);
		covid_c4_m10_5.setCodi_de_centre(c4_m10);
		covid_c4_m10_6.setCodi_de_centre(c4_m10);
		covid_c4_m10_7.setCodi_de_centre(c4_m10);
		
		covid_c5_m10_1.setCodi_de_centre(c5_m10);
		covid_c5_m10_2.setCodi_de_centre(c5_m10);
		covid_c5_m10_5.setCodi_de_centre(c5_m10);
		covid_c5_m10_6.setCodi_de_centre(c5_m10);
		covid_c5_m10_7.setCodi_de_centre(c5_m10);
		
		c1_m1.getCovid_centres().add(covid_c1_m1);
		c2_m1.getCovid_centres().add(covid_c2_m1);
		c3_m1.getCovid_centres().add(covid_c3_m1);
		c4_m1.getCovid_centres().add(covid_c4_m1);
		c5_m1.getCovid_centres().add(covid_c5_m1);
	
		c1_m2.getCovid_centres().add(covid_c1_m2);
		c2_m2.getCovid_centres().add(covid_c2_m2);
		
		c1_m3.getCovid_centres().add(covid_c1_m3);
		c2_m3.getCovid_centres().add(covid_c2_m3);
		c3_m3.getCovid_centres().add(covid_c3_m3);
		
		c1_m4.getCovid_centres().add(covid_c1_m4);
		c2_m4.getCovid_centres().add(covid_c2_m4);
		c3_m4.getCovid_centres().add(covid_c3_m4);
		c4_m4.getCovid_centres().add(covid_c4_m4);
		c5_m4.getCovid_centres().add(covid_c5_m4);
		
		c1_m5.getCovid_centres().add(covid_c1_m5);
		c2_m5.getCovid_centres().add(covid_c2_m5);
		c3_m5.getCovid_centres().add(covid_c3_m5);
		
		c1_m6.getCovid_centres().add(covid_c1_m6);
		c2_m6.getCovid_centres().add(covid_c2_m6);
		c3_m6.getCovid_centres().add(covid_c3_m6);
		
		c1_m7.getCovid_centres().add(covid_c1_m7);
		c2_m7.getCovid_centres().add(covid_c2_m7);
		c3_m7.getCovid_centres().add(covid_c3_m7);
		c4_m7.getCovid_centres().add(covid_c4_m7);
		c5_m7.getCovid_centres().add(covid_c5_m7);

		c1_m8.getCovid_centres().add(covid_c1_m8);
		c2_m8.getCovid_centres().add(covid_c2_m8);
		
		c1_m9.getCovid_centres().add(covid_c1_m9);
		c2_m9.getCovid_centres().add(covid_c2_m9);
		
		c1_m10.getCovid_centres().add(covid_c1_m10);
		c2_m10.getCovid_centres().add(covid_c2_m10);
		c3_m10.getCovid_centres().add(covid_c3_m10);
		c4_m10.getCovid_centres().add(covid_c4_m10);
		c5_m10.getCovid_centres().add(covid_c5_m10);
		
		c1_m1.getCovid_centres().add(covid_c1_m1_1);
		c1_m1.getCovid_centres().add(covid_c1_m1_2);
		c1_m1.getCovid_centres().add(covid_c1_m1_5);
		c1_m1.getCovid_centres().add(covid_c1_m1_6);
		c1_m1.getCovid_centres().add(covid_c1_m1_7);
		
		c2_m1.getCovid_centres().add(covid_c2_m1_1);
		c2_m1.getCovid_centres().add(covid_c2_m1_2);
		c2_m1.getCovid_centres().add(covid_c2_m1_5);
		c2_m1.getCovid_centres().add(covid_c2_m1_6);
		c2_m1.getCovid_centres().add(covid_c2_m1_7);
		
		c3_m1.getCovid_centres().add(covid_c3_m1_1);
		c3_m1.getCovid_centres().add(covid_c3_m1_2);
		c3_m1.getCovid_centres().add(covid_c3_m1_5);
		c3_m1.getCovid_centres().add(covid_c3_m1_6);
		c3_m1.getCovid_centres().add(covid_c3_m1_7);
		
		c4_m1.getCovid_centres().add(covid_c4_m1_1);
		c4_m1.getCovid_centres().add(covid_c4_m1_2);
		c4_m1.getCovid_centres().add(covid_c4_m1_5);
		c4_m1.getCovid_centres().add(covid_c4_m1_6);
		c4_m1.getCovid_centres().add(covid_c4_m1_7);
		
		c5_m1.getCovid_centres().add(covid_c5_m1_1);
		c5_m1.getCovid_centres().add(covid_c5_m1_2);
		c5_m1.getCovid_centres().add(covid_c5_m1_5);
		c5_m1.getCovid_centres().add(covid_c5_m1_6);
		c5_m1.getCovid_centres().add(covid_c5_m1_7);
		
		c1_m2.getCovid_centres().add(covid_c1_m2_1);
		c1_m2.getCovid_centres().add(covid_c1_m2_2);
		c1_m2.getCovid_centres().add(covid_c1_m2_5);
		c1_m2.getCovid_centres().add(covid_c1_m2_6);
		c1_m2.getCovid_centres().add(covid_c1_m2_7);
		
		c2_m2.getCovid_centres().add(covid_c2_m2_1);
		c2_m2.getCovid_centres().add(covid_c2_m2_2);
		c2_m2.getCovid_centres().add(covid_c2_m2_5);
		c2_m2.getCovid_centres().add(covid_c2_m2_6);
		c2_m2.getCovid_centres().add(covid_c2_m2_7);
		
		c1_m3.getCovid_centres().add(covid_c1_m3_1);
		c1_m3.getCovid_centres().add(covid_c1_m3_2);
		c1_m3.getCovid_centres().add(covid_c1_m3_5);
		c1_m3.getCovid_centres().add(covid_c1_m3_6);
		c1_m3.getCovid_centres().add(covid_c1_m3_7);
		
		c2_m3.getCovid_centres().add(covid_c2_m3_1);
		c2_m3.getCovid_centres().add(covid_c2_m3_2);
		c2_m3.getCovid_centres().add(covid_c2_m3_5);
		c2_m3.getCovid_centres().add(covid_c2_m3_6);
		c2_m3.getCovid_centres().add(covid_c2_m3_7);
		
		c3_m3.getCovid_centres().add(covid_c3_m3_1);
		c3_m3.getCovid_centres().add(covid_c3_m3_2);
		c3_m3.getCovid_centres().add(covid_c3_m3_5);
		c3_m3.getCovid_centres().add(covid_c3_m3_6);
		c3_m3.getCovid_centres().add(covid_c3_m3_7);
		
		c1_m4.getCovid_centres().add(covid_c1_m4_1);
		c1_m4.getCovid_centres().add(covid_c1_m4_2);
		c1_m4.getCovid_centres().add(covid_c1_m4_5);
		c1_m4.getCovid_centres().add(covid_c1_m4_6);
		c1_m4.getCovid_centres().add(covid_c1_m4_7);
		
		c2_m4.getCovid_centres().add(covid_c2_m4_1);
		c2_m4.getCovid_centres().add(covid_c2_m4_2);
		c2_m4.getCovid_centres().add(covid_c2_m4_5);
		c2_m4.getCovid_centres().add(covid_c2_m4_6);
		c2_m4.getCovid_centres().add(covid_c2_m4_7);
		
		c3_m4.getCovid_centres().add(covid_c3_m4_1);
		c3_m4.getCovid_centres().add(covid_c3_m4_2);
		c3_m4.getCovid_centres().add(covid_c3_m4_5);
		c3_m4.getCovid_centres().add(covid_c3_m4_6);
		c3_m4.getCovid_centres().add(covid_c3_m4_7);
		
		c4_m4.getCovid_centres().add(covid_c4_m4_1);
		c4_m4.getCovid_centres().add(covid_c4_m4_2);
		c4_m4.getCovid_centres().add(covid_c4_m4_5);
		c4_m4.getCovid_centres().add(covid_c4_m4_6);
		c4_m4.getCovid_centres().add(covid_c4_m4_7);
		
		c5_m4.getCovid_centres().add(covid_c5_m4_1);
		c5_m4.getCovid_centres().add(covid_c5_m4_2);
		c5_m4.getCovid_centres().add(covid_c5_m4_5);
		c5_m4.getCovid_centres().add(covid_c5_m4_6);
		c5_m4.getCovid_centres().add(covid_c5_m4_7);
		
		c1_m5.getCovid_centres().add(covid_c1_m5_1);
		c1_m5.getCovid_centres().add(covid_c1_m5_2);
		c1_m5.getCovid_centres().add(covid_c1_m5_5);
		c1_m5.getCovid_centres().add(covid_c1_m5_6);
		c1_m5.getCovid_centres().add(covid_c1_m5_7);
		
		c2_m5.getCovid_centres().add(covid_c2_m5_1);
		c2_m5.getCovid_centres().add(covid_c2_m5_2);
		c2_m5.getCovid_centres().add(covid_c2_m5_5);
		c2_m5.getCovid_centres().add(covid_c2_m5_6);
		c2_m5.getCovid_centres().add(covid_c2_m5_7);
		
		c3_m5.getCovid_centres().add(covid_c3_m5_1);
		c3_m5.getCovid_centres().add(covid_c3_m5_2);
		c3_m5.getCovid_centres().add(covid_c3_m5_5);
		c3_m5.getCovid_centres().add(covid_c3_m5_6);
		c3_m5.getCovid_centres().add(covid_c3_m5_7);		
		
		c1_m6.getCovid_centres().add(covid_c1_m6_1);
		c1_m6.getCovid_centres().add(covid_c1_m6_2);
		c1_m6.getCovid_centres().add(covid_c1_m6_5);
		c1_m6.getCovid_centres().add(covid_c1_m6_6);
		c1_m6.getCovid_centres().add(covid_c1_m6_7);
		
		c2_m6.getCovid_centres().add(covid_c2_m6_1);
		c2_m6.getCovid_centres().add(covid_c2_m6_2);
		c2_m6.getCovid_centres().add(covid_c2_m6_5);
		c2_m6.getCovid_centres().add(covid_c2_m6_6);
		c2_m6.getCovid_centres().add(covid_c2_m6_7);
		
		c3_m6.getCovid_centres().add(covid_c3_m6_1);
		c3_m6.getCovid_centres().add(covid_c3_m6_2);
		c3_m6.getCovid_centres().add(covid_c3_m6_5);
		c3_m6.getCovid_centres().add(covid_c3_m6_6);
		c3_m6.getCovid_centres().add(covid_c3_m6_7);
		
		c1_m7.getCovid_centres().add(covid_c1_m7_1);
		c1_m7.getCovid_centres().add(covid_c1_m7_2);
		c1_m7.getCovid_centres().add(covid_c1_m7_5);
		c1_m7.getCovid_centres().add(covid_c1_m7_6);
		c1_m7.getCovid_centres().add(covid_c1_m7_7);
		
		c2_m7.getCovid_centres().add(covid_c2_m7_1);
		c2_m7.getCovid_centres().add(covid_c2_m7_2);
		c2_m7.getCovid_centres().add(covid_c2_m7_5);
		c2_m7.getCovid_centres().add(covid_c2_m7_6);
		c2_m7.getCovid_centres().add(covid_c2_m7_7);
		
		c3_m7.getCovid_centres().add(covid_c3_m7_1);
		c3_m7.getCovid_centres().add(covid_c3_m7_2);
		c3_m7.getCovid_centres().add(covid_c3_m7_5);
		c3_m7.getCovid_centres().add(covid_c3_m7_6);
		c3_m7.getCovid_centres().add(covid_c3_m7_7);
		
		c4_m7.getCovid_centres().add(covid_c4_m7_1);
		c4_m7.getCovid_centres().add(covid_c4_m7_2);
		c4_m7.getCovid_centres().add(covid_c4_m7_5);
		c4_m7.getCovid_centres().add(covid_c4_m7_6);
		c4_m7.getCovid_centres().add(covid_c4_m7_7);
		
		c5_m7.getCovid_centres().add(covid_c5_m7_1);
		c5_m7.getCovid_centres().add(covid_c5_m7_2);
		c5_m7.getCovid_centres().add(covid_c5_m7_5);
		c5_m7.getCovid_centres().add(covid_c5_m7_6);
		c5_m7.getCovid_centres().add(covid_c5_m7_7);
		
		c1_m8.getCovid_centres().add(covid_c1_m8_1);
		c1_m8.getCovid_centres().add(covid_c1_m8_2);
		c1_m8.getCovid_centres().add(covid_c1_m8_5);
		c1_m8.getCovid_centres().add(covid_c1_m8_6);
		c1_m8.getCovid_centres().add(covid_c1_m8_7);
		
		c2_m8.getCovid_centres().add(covid_c2_m8_1);
		c2_m8.getCovid_centres().add(covid_c2_m8_2);
		c2_m8.getCovid_centres().add(covid_c2_m8_5);
		c2_m8.getCovid_centres().add(covid_c2_m8_6);
		c2_m8.getCovid_centres().add(covid_c2_m8_7);
		
		c1_m9.getCovid_centres().add(covid_c1_m9_1);
		c1_m9.getCovid_centres().add(covid_c1_m9_2);
		c1_m9.getCovid_centres().add(covid_c1_m9_5);
		c1_m9.getCovid_centres().add(covid_c1_m9_6);
		c1_m9.getCovid_centres().add(covid_c1_m9_7);
		
		c2_m9.getCovid_centres().add(covid_c2_m9_1);
		c2_m9.getCovid_centres().add(covid_c2_m9_2);
		c2_m9.getCovid_centres().add(covid_c2_m9_5);
		c2_m9.getCovid_centres().add(covid_c2_m9_6);
		c2_m9.getCovid_centres().add(covid_c2_m9_7);
		
		c1_m10.getCovid_centres().add(covid_c1_m10_1);
		c1_m10.getCovid_centres().add(covid_c1_m10_2);
		c1_m10.getCovid_centres().add(covid_c1_m10_5);
		c1_m10.getCovid_centres().add(covid_c1_m10_6);
		c1_m10.getCovid_centres().add(covid_c1_m10_7);
		
		c2_m10.getCovid_centres().add(covid_c2_m10_1);
		c2_m10.getCovid_centres().add(covid_c2_m10_2);
		c2_m10.getCovid_centres().add(covid_c2_m10_5);
		c2_m10.getCovid_centres().add(covid_c2_m10_6);
		c2_m10.getCovid_centres().add(covid_c2_m10_7);
		
		c3_m10.getCovid_centres().add(covid_c3_m10_1);
		c3_m10.getCovid_centres().add(covid_c3_m10_2);
		c3_m10.getCovid_centres().add(covid_c3_m10_5);
		c3_m10.getCovid_centres().add(covid_c3_m10_6);
		c3_m10.getCovid_centres().add(covid_c3_m10_7);
		
		c4_m10.getCovid_centres().add(covid_c4_m10_1);
		c4_m10.getCovid_centres().add(covid_c4_m10_2);
		c4_m10.getCovid_centres().add(covid_c4_m10_5);
		c4_m10.getCovid_centres().add(covid_c4_m10_6);
		c4_m10.getCovid_centres().add(covid_c4_m10_7);
		
		c5_m10.getCovid_centres().add(covid_c5_m10_1);
		c5_m10.getCovid_centres().add(covid_c5_m10_2);
		c5_m10.getCovid_centres().add(covid_c5_m10_5);
		c5_m10.getCovid_centres().add(covid_c5_m10_6);
		c5_m10.getCovid_centres().add(covid_c5_m10_7);
		
		municipisDAO.saveOrUpdate(m1);
		municipisDAO.saveOrUpdate(m2);
		municipisDAO.saveOrUpdate(m3);
		municipisDAO.saveOrUpdate(m4);
		municipisDAO.saveOrUpdate(m5);
		municipisDAO.saveOrUpdate(m6);
		municipisDAO.saveOrUpdate(m7);
		municipisDAO.saveOrUpdate(m8);
		municipisDAO.saveOrUpdate(m9);
		municipisDAO.saveOrUpdate(m10);
		usuariDAO.saveOrUpdate(u1);
		usuariDAO.saveOrUpdate(u2);
		usuariDAO.saveOrUpdate(u3);
		centresDAO.saveOrUpdate(c1_m1);
		centresDAO.saveOrUpdate(c2_m1);
		centresDAO.saveOrUpdate(c3_m1);
		centresDAO.saveOrUpdate(c4_m1);
		centresDAO.saveOrUpdate(c5_m1);
		centresDAO.saveOrUpdate(c1_m2);
		centresDAO.saveOrUpdate(c2_m2);
		centresDAO.saveOrUpdate(c1_m3);
		centresDAO.saveOrUpdate(c2_m3);
		centresDAO.saveOrUpdate(c3_m3);
		centresDAO.saveOrUpdate(c1_m4);
		centresDAO.saveOrUpdate(c2_m4);
		centresDAO.saveOrUpdate(c3_m4);
		centresDAO.saveOrUpdate(c4_m4);
		centresDAO.saveOrUpdate(c5_m4);
		centresDAO.saveOrUpdate(c1_m5);
		centresDAO.saveOrUpdate(c2_m5);
		centresDAO.saveOrUpdate(c3_m5);
		centresDAO.saveOrUpdate(c1_m6);
		centresDAO.saveOrUpdate(c2_m6);
		centresDAO.saveOrUpdate(c3_m6);
		centresDAO.saveOrUpdate(c1_m7);
		centresDAO.saveOrUpdate(c2_m7);
		centresDAO.saveOrUpdate(c3_m7);
		centresDAO.saveOrUpdate(c4_m7);
		centresDAO.saveOrUpdate(c5_m7);
		centresDAO.saveOrUpdate(c1_m8);
		centresDAO.saveOrUpdate(c2_m8);
		centresDAO.saveOrUpdate(c1_m9);
		centresDAO.saveOrUpdate(c2_m9);
		centresDAO.saveOrUpdate(c1_m10);
		centresDAO.saveOrUpdate(c2_m10);
		centresDAO.saveOrUpdate(c3_m10);
		centresDAO.saveOrUpdate(c4_m10);
		centresDAO.saveOrUpdate(c5_m10);
		covidDAO.saveOrUpdate(covid_c1_m1);
		covidDAO.saveOrUpdate(covid_c2_m1);
		covidDAO.saveOrUpdate(covid_c3_m1);
		covidDAO.saveOrUpdate(covid_c4_m1);
		covidDAO.saveOrUpdate(covid_c5_m1);
		covidDAO.saveOrUpdate(covid_c1_m2);
		covidDAO.saveOrUpdate(covid_c2_m2);
		covidDAO.saveOrUpdate(covid_c1_m3);
		covidDAO.saveOrUpdate(covid_c2_m3);
		covidDAO.saveOrUpdate(covid_c3_m3);
		covidDAO.saveOrUpdate(covid_c1_m4);
		covidDAO.saveOrUpdate(covid_c2_m4);
		covidDAO.saveOrUpdate(covid_c3_m4);
		covidDAO.saveOrUpdate(covid_c4_m4);
		covidDAO.saveOrUpdate(covid_c5_m4);
		covidDAO.saveOrUpdate(covid_c1_m5);
		covidDAO.saveOrUpdate(covid_c2_m5);
		covidDAO.saveOrUpdate(covid_c3_m5);
		covidDAO.saveOrUpdate(covid_c1_m6);
		covidDAO.saveOrUpdate(covid_c2_m6);
		covidDAO.saveOrUpdate(covid_c3_m6);	
		covidDAO.saveOrUpdate(covid_c1_m7);
		covidDAO.saveOrUpdate(covid_c2_m7);
		covidDAO.saveOrUpdate(covid_c3_m7);
		covidDAO.saveOrUpdate(covid_c4_m7);
		covidDAO.saveOrUpdate(covid_c5_m7);
		covidDAO.saveOrUpdate(covid_c1_m8);
		covidDAO.saveOrUpdate(covid_c2_m8);
		covidDAO.saveOrUpdate(covid_c1_m9);
		covidDAO.saveOrUpdate(covid_c2_m9);
		covidDAO.saveOrUpdate(covid_c1_m10);
		covidDAO.saveOrUpdate(covid_c2_m10);
		covidDAO.saveOrUpdate(covid_c3_m10);
		covidDAO.saveOrUpdate(covid_c4_m10);
		covidDAO.saveOrUpdate(covid_c5_m10);
		
		covidDAO.saveOrUpdate(covid_c1_m1_1);
		covidDAO.saveOrUpdate(covid_c1_m1_2);
		covidDAO.saveOrUpdate(covid_c1_m1_5);
		covidDAO.saveOrUpdate(covid_c1_m1_6);
		covidDAO.saveOrUpdate(covid_c1_m1_7);
		covidDAO.saveOrUpdate(covid_c2_m1_1);
		covidDAO.saveOrUpdate(covid_c2_m1_2);
		covidDAO.saveOrUpdate(covid_c2_m1_5);
		covidDAO.saveOrUpdate(covid_c2_m1_6);
		covidDAO.saveOrUpdate(covid_c2_m1_7);
		covidDAO.saveOrUpdate(covid_c3_m1_1);
		covidDAO.saveOrUpdate(covid_c3_m1_2);
		covidDAO.saveOrUpdate(covid_c3_m1_5);
		covidDAO.saveOrUpdate(covid_c3_m1_6);
		covidDAO.saveOrUpdate(covid_c3_m1_7);
		covidDAO.saveOrUpdate(covid_c4_m1_1);
		covidDAO.saveOrUpdate(covid_c4_m1_2);
		covidDAO.saveOrUpdate(covid_c4_m1_5);
		covidDAO.saveOrUpdate(covid_c4_m1_6);
		covidDAO.saveOrUpdate(covid_c4_m1_7);
		covidDAO.saveOrUpdate(covid_c5_m1_1);
		covidDAO.saveOrUpdate(covid_c5_m1_2);
		covidDAO.saveOrUpdate(covid_c5_m1_5);
		covidDAO.saveOrUpdate(covid_c5_m1_6);
		covidDAO.saveOrUpdate(covid_c5_m1_7);
		covidDAO.saveOrUpdate(covid_c1_m2_1);
		covidDAO.saveOrUpdate(covid_c1_m2_2);
		covidDAO.saveOrUpdate(covid_c1_m2_5);
		covidDAO.saveOrUpdate(covid_c1_m2_6);
		covidDAO.saveOrUpdate(covid_c1_m2_7);
		covidDAO.saveOrUpdate(covid_c2_m2_1);
		covidDAO.saveOrUpdate(covid_c2_m2_2);
		covidDAO.saveOrUpdate(covid_c2_m2_5);
		covidDAO.saveOrUpdate(covid_c2_m2_6);
		covidDAO.saveOrUpdate(covid_c2_m2_7);
		covidDAO.saveOrUpdate(covid_c1_m3_1);
		covidDAO.saveOrUpdate(covid_c1_m3_2);
		covidDAO.saveOrUpdate(covid_c1_m3_5);
		covidDAO.saveOrUpdate(covid_c1_m3_6);
		covidDAO.saveOrUpdate(covid_c1_m3_7);
		covidDAO.saveOrUpdate(covid_c2_m3_1);
		covidDAO.saveOrUpdate(covid_c2_m3_2);
		covidDAO.saveOrUpdate(covid_c2_m3_5);
		covidDAO.saveOrUpdate(covid_c2_m3_6);
		covidDAO.saveOrUpdate(covid_c2_m3_7);
		covidDAO.saveOrUpdate(covid_c3_m3_1);
		covidDAO.saveOrUpdate(covid_c3_m3_2);
		covidDAO.saveOrUpdate(covid_c3_m3_5);
		covidDAO.saveOrUpdate(covid_c3_m3_6);
		covidDAO.saveOrUpdate(covid_c3_m3_7);
		covidDAO.saveOrUpdate(covid_c1_m4_1);
		covidDAO.saveOrUpdate(covid_c1_m4_2);
		covidDAO.saveOrUpdate(covid_c1_m4_5);
		covidDAO.saveOrUpdate(covid_c1_m4_6);
		covidDAO.saveOrUpdate(covid_c1_m4_7);
		covidDAO.saveOrUpdate(covid_c2_m4_1);
		covidDAO.saveOrUpdate(covid_c2_m4_2);
		covidDAO.saveOrUpdate(covid_c2_m4_5);
		covidDAO.saveOrUpdate(covid_c2_m4_6);
		covidDAO.saveOrUpdate(covid_c2_m4_7);
		covidDAO.saveOrUpdate(covid_c3_m4_1);
		covidDAO.saveOrUpdate(covid_c3_m4_2);
		covidDAO.saveOrUpdate(covid_c3_m4_5);
		covidDAO.saveOrUpdate(covid_c3_m4_6);
		covidDAO.saveOrUpdate(covid_c3_m4_7);
		covidDAO.saveOrUpdate(covid_c4_m4_1);
		covidDAO.saveOrUpdate(covid_c4_m4_2);
		covidDAO.saveOrUpdate(covid_c4_m4_5);
		covidDAO.saveOrUpdate(covid_c4_m4_6);
		covidDAO.saveOrUpdate(covid_c4_m4_7);
		covidDAO.saveOrUpdate(covid_c5_m4_1);
		covidDAO.saveOrUpdate(covid_c5_m4_2);
		covidDAO.saveOrUpdate(covid_c5_m4_5);
		covidDAO.saveOrUpdate(covid_c5_m4_6);
		covidDAO.saveOrUpdate(covid_c5_m4_7);
		covidDAO.saveOrUpdate(covid_c1_m5_1);
		covidDAO.saveOrUpdate(covid_c1_m5_2);
		covidDAO.saveOrUpdate(covid_c1_m5_5);
		covidDAO.saveOrUpdate(covid_c1_m5_6);
		covidDAO.saveOrUpdate(covid_c1_m5_7);
		covidDAO.saveOrUpdate(covid_c2_m5_1);
		covidDAO.saveOrUpdate(covid_c2_m5_2);
		covidDAO.saveOrUpdate(covid_c2_m5_5);
		covidDAO.saveOrUpdate(covid_c2_m5_6);
		covidDAO.saveOrUpdate(covid_c2_m5_7);
		covidDAO.saveOrUpdate(covid_c3_m5_1);
		covidDAO.saveOrUpdate(covid_c3_m5_2);
		covidDAO.saveOrUpdate(covid_c3_m5_5);
		covidDAO.saveOrUpdate(covid_c3_m5_6);
		covidDAO.saveOrUpdate(covid_c3_m5_7);		
		covidDAO.saveOrUpdate(covid_c1_m6_1);
		covidDAO.saveOrUpdate(covid_c1_m6_2);
		covidDAO.saveOrUpdate(covid_c1_m6_5);
		covidDAO.saveOrUpdate(covid_c1_m6_6);
		covidDAO.saveOrUpdate(covid_c1_m6_7);
		covidDAO.saveOrUpdate(covid_c2_m6_1);
		covidDAO.saveOrUpdate(covid_c2_m6_2);
		covidDAO.saveOrUpdate(covid_c2_m6_5);
		covidDAO.saveOrUpdate(covid_c2_m6_6);
		covidDAO.saveOrUpdate(covid_c2_m6_7);
		covidDAO.saveOrUpdate(covid_c3_m6_1);
		covidDAO.saveOrUpdate(covid_c3_m6_2);
		covidDAO.saveOrUpdate(covid_c3_m6_5);
		covidDAO.saveOrUpdate(covid_c3_m6_6);
		covidDAO.saveOrUpdate(covid_c3_m6_7);
		covidDAO.saveOrUpdate(covid_c1_m7_1);
		covidDAO.saveOrUpdate(covid_c1_m7_2);
		covidDAO.saveOrUpdate(covid_c1_m7_5);
		covidDAO.saveOrUpdate(covid_c1_m7_6);
		covidDAO.saveOrUpdate(covid_c1_m7_7);
		covidDAO.saveOrUpdate(covid_c2_m7_1);
		covidDAO.saveOrUpdate(covid_c2_m7_2);
		covidDAO.saveOrUpdate(covid_c2_m7_5);
		covidDAO.saveOrUpdate(covid_c2_m7_6);
		covidDAO.saveOrUpdate(covid_c2_m7_7);
		covidDAO.saveOrUpdate(covid_c3_m7_1);
		covidDAO.saveOrUpdate(covid_c3_m7_2);
		covidDAO.saveOrUpdate(covid_c3_m7_5);
		covidDAO.saveOrUpdate(covid_c3_m7_6);
		covidDAO.saveOrUpdate(covid_c3_m7_7);
		covidDAO.saveOrUpdate(covid_c4_m7_1);
		covidDAO.saveOrUpdate(covid_c4_m7_2);
		covidDAO.saveOrUpdate(covid_c4_m7_5);
		covidDAO.saveOrUpdate(covid_c4_m7_6);
		covidDAO.saveOrUpdate(covid_c4_m7_7);
		covidDAO.saveOrUpdate(covid_c5_m7_1);
		covidDAO.saveOrUpdate(covid_c5_m7_2);
		covidDAO.saveOrUpdate(covid_c5_m7_5);
		covidDAO.saveOrUpdate(covid_c5_m7_6);
		covidDAO.saveOrUpdate(covid_c5_m7_7);
		covidDAO.saveOrUpdate(covid_c1_m8_1);
		covidDAO.saveOrUpdate(covid_c1_m8_2);
		covidDAO.saveOrUpdate(covid_c1_m8_5);
		covidDAO.saveOrUpdate(covid_c1_m8_6);
		covidDAO.saveOrUpdate(covid_c1_m8_7);
		covidDAO.saveOrUpdate(covid_c2_m8_1);
		covidDAO.saveOrUpdate(covid_c2_m8_2);
		covidDAO.saveOrUpdate(covid_c2_m8_5);
		covidDAO.saveOrUpdate(covid_c2_m8_6);
		covidDAO.saveOrUpdate(covid_c2_m8_7);
		covidDAO.saveOrUpdate(covid_c1_m9_1);
		covidDAO.saveOrUpdate(covid_c1_m9_2);
		covidDAO.saveOrUpdate(covid_c1_m9_5);
		covidDAO.saveOrUpdate(covid_c1_m9_6);
		covidDAO.saveOrUpdate(covid_c1_m9_7);
		covidDAO.saveOrUpdate(covid_c2_m9_1);
		covidDAO.saveOrUpdate(covid_c2_m9_2);
		covidDAO.saveOrUpdate(covid_c2_m9_5);
		covidDAO.saveOrUpdate(covid_c2_m9_6);
		covidDAO.saveOrUpdate(covid_c2_m9_7);
		covidDAO.saveOrUpdate(covid_c1_m10_1);
		covidDAO.saveOrUpdate(covid_c1_m10_2);
		covidDAO.saveOrUpdate(covid_c1_m10_5);
		covidDAO.saveOrUpdate(covid_c1_m10_6);
		covidDAO.saveOrUpdate(covid_c1_m10_7);
		covidDAO.saveOrUpdate(covid_c2_m10_1);
		covidDAO.saveOrUpdate(covid_c2_m10_2);
		covidDAO.saveOrUpdate(covid_c2_m10_5);
		covidDAO.saveOrUpdate(covid_c2_m10_6);
		covidDAO.saveOrUpdate(covid_c2_m10_7);
		covidDAO.saveOrUpdate(covid_c3_m10_1);
		covidDAO.saveOrUpdate(covid_c3_m10_2);
		covidDAO.saveOrUpdate(covid_c3_m10_5);
		covidDAO.saveOrUpdate(covid_c3_m10_6);
		covidDAO.saveOrUpdate(covid_c3_m10_7);
		covidDAO.saveOrUpdate(covid_c4_m10_1);
		covidDAO.saveOrUpdate(covid_c4_m10_2);
		covidDAO.saveOrUpdate(covid_c4_m10_5);
		covidDAO.saveOrUpdate(covid_c4_m10_6);
		covidDAO.saveOrUpdate(covid_c4_m10_7);
		covidDAO.saveOrUpdate(covid_c5_m10_1);
		covidDAO.saveOrUpdate(covid_c5_m10_2);
		covidDAO.saveOrUpdate(covid_c5_m10_5);
		covidDAO.saveOrUpdate(covid_c5_m10_6);
		covidDAO.saveOrUpdate(covid_c5_m10_7);
		
		Estadistiques_Municipis e = new Estadistiques_Municipis();
		e.setAcumulat_dies(1);
		e.setAlerta_creada(true);
		e.setCodi_municipi(m1);
		e.setData_generacio(new Date(120,0,1));
		e.setGenerar_alerta(true);
		e.setIncidencia_acumulada(20);
		e.setNous_positius(1000);
		e.setPositius_n1(30);
		e.setPositius_n2(1000);
		e.setPositius_n5(40);
		e.setPositius_n6(30);
		e.setPositius_n7(80);
		e.setRealizada(false);
		e.setRisc_rebrot(40);
		e.setVelocitat_propagacio(70);
		estadistiquesDAO.saveOrUpdate(e);
		
	}
	
}

class Escaner {
	
private Scanner sc;
	
	/**
	 * Constructor clase escaner
	 */
	public Escaner() {
		sc = new Scanner(System.in);
	}
	
	/**
	 * Introdueix text
	 * @param texto Text que mostrar� al "syso" per informar
	 * @return Cadena de text introduida
	 */
	public String text(String texto) {
        String text;
        do {
            System.out.println("Introdueix " + texto);
            text = sc.nextLine();
            if (text.trim().length() < 1)
            	System.out.println("Aquest camp no pot estar buit");
        } while(text.trim().length() < 1);
        return text;
    }
	
	/**
	 * Introdueix un numero 
	 * @param texto Text que mostrar� al "syso" per informar
	 * @param min Numero m�nim a introduir
	 * @param max Numero m�xim a introduir
	 * @return Numero introduit
	 */
	public int numero(String texto, int min, int max) {
		int num = 0;
		boolean cen = false;
		while (!cen) {
			System.out.println("Introdueix " + texto);
			if (sc.hasNextInt()) {
				num = sc.nextInt();
				if (num >= min && num <= max) {
					cen = true;
					sc.nextLine();
				}
				else
					System.out.println("El numero ha de ser com a m�nim: " + min + " i com a m�xim: " + max);
			} else {
				sc.next();
				System.out.println("Introduce un numero");
			}
		}
		return num;
	}
	
	/**
	 * Metode per a tancar l'escaner
	 */
	public void close() {
		sc.close();
	}

}
