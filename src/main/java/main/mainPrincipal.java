package main;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import model.COVID_Centres_docents;
import model.Centres_Docents;
import model.Municipis;
import model.Usuari;



public class mainPrincipal {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void main(String[] args) {
		
		Date data = new Date();
		
		try {
			session = getSessionFactory().openSession();
			
			session.beginTransaction();
			
			Municipis m1 = new Municipis(81878, "Sabadell", 40, "Vall�s Occidental", 213644); // 4, 22, 45, 52, 19
			Municipis m2 = new Municipis(81672, "Poliny�", 40, "Vall�s Occidental", 8479); // 0, 0, 2, 1, 0
			Municipis m3 = new Municipis(81568, "Palau-solit� i Plegamans", 40, "Vall�s Occidental", 14771); // 0, 4, 2, 2, 0
			Municipis m4 = new Municipis(82606, "Santa Perp�tua de Mogoda", 40, "Vall�s Occidental", 25799); // 4, 1, 5, 7, 0
			Municipis m5 = new Municipis(80517, "Castellar del Vall�s", 40, "Vall�s Occidental", 24187); // 0, 1, 0, 4, 0
			Municipis m6 = new Municipis(82520, "Barber� del Vall�s", 40, "Vall�s Occidental", 33091); // 1, 7, 5, 6, 5
			Municipis m7 = new Municipis(82798, "Terrassa", 40, "Vall�s Occidental", 220556); // 7, 32, 26, 34, 8
			Municipis m8 = new Municipis(82671, "Sentmenat", 40, "Vall�s Occidental", 9078); // 0, 0, 0, 0, 0, 1
			Municipis m9 = new Municipis(89045, "Badia del Vall�s", 40, "Vall�s Occidental", 13380); // 0, 2, 5, 3, 0
			Municipis m10 = new Municipis(82665, "Cerdanyola del Vall�s", 40, "Vall�s Occidental", 57403); // 1, 2, 5, 8, 3
			
			Usuari u1 = new Usuari("Isaac", "Garc�a", "Jim�nez");
			Usuari u2 = new Usuari("Oriol", "Porta", "Sobrino");
			Usuari u3 = new Usuari("Eloi", "Vazquez", "Batista");
			
			Centres_Docents c1_m1 = new Centres_Docents(8044624, "Institut Sabadell", "c. Juvenal, 1", "08206"); 
			Centres_Docents c2_m1 = new Centres_Docents(8024741, "Institut Escola Industrial", "c. Calder�n, 56", "08201");
			Centres_Docents c3_m1 = new Centres_Docents(8046669, "Institut Ribot i Serra", "c. Concha Espina, 33", "08204");
			Centres_Docents c4_m1 = new Centres_Docents(8024546, "Sant Francesc", "c. Pizarro, 18", "08204");
			Centres_Docents c5_m1 = new Centres_Docents(8055105, "Escola Arraona", "c. Argentina, 45", "08205");			
			
			Centres_Docents c1_m2 = new Centres_Docents(8053170, "Institut Poliny�","av. Sabadell, 1-3", "08213");
			Centres_Docents c2_m2 = new Centres_Docents(8022872, "Escola Pere Calders", "pg. Sanllehy, s/n", "08213");
			
			Centres_Docents c1_m3 = new Centres_Docents(8045306, "Institut Ramon Casas i Carb�", "c. Lluis Companys, 2", "08184");
			Centres_Docents c2_m3 = new Centres_Docents(8022616, "Escola Josep Maria Folch i Torres", "c. Folch i Torras, 30", "08184");
			Centres_Docents c3_m3 = new Centres_Docents(8053467, "Escola Palau", "c. Arquitecte Falguera, 37", "08184");
			
			Centres_Docents c1_m4 = new Centres_Docents(8045021, "Institut Estela Ib�rica", "c. Passatge de mas Granollacs, s/n", "08130");
			Centres_Docents c2_m4 = new Centres_Docents(8034461, "Escola Bernat de Mogoda", "c. Pau Picasso, 2", "08130");
			Centres_Docents c3_m4 = new Centres_Docents(8053236, "Institut Rovira-Forns", "c. Tierno Galvan, 77", "08130");
			Centres_Docents c4_m4 = new Centres_Docents(8028485, "Sagrada Fam�lia", "c. Puig i Cadafalch, s/n", "08130");
			Centres_Docents c5_m4 = new Centres_Docents(8028497, "Escola Santa Perp�tua", "c. de Tierno Galv�n, 79", "08130");
			
			Centres_Docents c1_m5 = new Centres_Docents(8076558, "Institut Escola Sant Esteve", "c. Prat de la Riba, s/n", "08211");
			Centres_Docents c2_m5 = new Centres_Docents(8046682, "Institut de Castellar", "c. Carrasco i Formiguera, 6", "08211");
			Centres_Docents c3_m5 = new Centres_Docents(8054824, "Escola Mestre Pla", "c. Josep Carner, 2", "08211");	
			
			Centres_Docents c1_m6 = new Centres_Docents(8076546, "Institut Escola Can Llobet", "ptge. Dr. Moragas, 243", "08210");
			Centres_Docents c2_m6 = new Centres_Docents(8035349, "Institut La Rom�nica", "rda. de Santa Maria, 310", "08210");
			Centres_Docents c3_m6 = new Centres_Docents(8039641, "Escola Pablo Picasso", "c. Can�ries, 26", "08210");

			Centres_Docents c1_m7 = new Centres_Docents(8076601, "Institut Escola Pere Viver", "C. Arenys de Mar, 2", "08221");
			Centres_Docents c2_m7 = new Centres_Docents(8034059, "Institut Nicolau Cop�rnic", "c. del Torrent del Batlle, 10", "08225");
			Centres_Docents c3_m7 = new Centres_Docents(8030078, "Egara", "ctra. de Castellar, 126", "08222");
			Centres_Docents c4_m7 = new Centres_Docents(8024777, "Institut Egara", "c. Am�rica, 55 (Can Parellada)", "08228");
			Centres_Docents c5_m7 = new Centres_Docents(8031770, "Institut Can Jofresa", "av. Can Jofresa, 9", "08223");
			
			Centres_Docents c1_m8 = new Centres_Docents(8053248, "Institut de Sentmenat", "c. Poca Farina, s/n", "08181");
			Centres_Docents c2_m8 = new Centres_Docents(8028990, "Escola Can Sorts", "c. Joanot Martorell, s/n", "08181");

			Centres_Docents c1_m9 = new Centres_Docents(8042342, "Institut de Badia del Vall�s", "c. Mallorca, s/n", "08214");
			Centres_Docents c2_m9 = new Centres_Docents(8034035, "Institut Federica Montseny", "c. Oporto, s/n", "08214");			

			Centres_Docents c1_m10 = new Centres_Docents(8043504, "Institut Pere Calders", "Campus U.A.B.", "08193");
			Centres_Docents c2_m10 = new Centres_Docents(8045549, "Institut Ban�s", "c. Sant Casimir, 16", "08290");	
			Centres_Docents c3_m10 = new Centres_Docents(8028849, "Escaladei", "c. Sant Salvador, 8", "08290");
			Centres_Docents c4_m10 = new Centres_Docents(8031757, "Institut Forat del Vent", "Pizarro, 35", "08290");
			Centres_Docents c5_m10 = new Centres_Docents(8028886, "Montserrat", "c. Sant Iscle, 6", "08290");
			
			COVID_Centres_docents covid_c1_m1 = new COVID_Centres_docents("Obert", data, 3, 43, 3, 0, null, 29, 1, 0, 3, 2, 0, 0);
			COVID_Centres_docents covid_c2_m1 = new COVID_Centres_docents("Obert", data, 1, 19, 1, 0, null, 37, 1, 0, 1, 3, 0, 0);
			COVID_Centres_docents covid_c3_m1 = new COVID_Centres_docents("Obert", data, 3, 48, 1, 0, null, 36, 0, 0, 3, 3, 0, 0);
			COVID_Centres_docents covid_c4_m1 = new COVID_Centres_docents("Obert", data, 2, 43, 2, 0, null, 7, 2, 0, 2, 3, 0, 0);
			COVID_Centres_docents covid_c5_m1 = new COVID_Centres_docents("Obert", data, 2, 35, 2, 0, null, 8, 1, 0, 2, 3, 0, 0);
			
			COVID_Centres_docents covid_c1_m2 = new COVID_Centres_docents("Obert", data , 0, 0, 0, 0, null, 4, 0, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m2 = new COVID_Centres_docents("Obert", data , 2, 34, 3, 0, null, 4, 0, 1, 2, 0, 0, 0);
			
			COVID_Centres_docents covid_c1_m3 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 9, 0, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m3 = new COVID_Centres_docents("Obert", data, 1, 17, 1, 0, null, 3, 0, 0, 1, 3, 0, 0);
			COVID_Centres_docents covid_c3_m3 = new COVID_Centres_docents("Obert", data, 2, 33, 4, 0, null, 5, 2, 0, 2, 2, 0, 0);
			
			COVID_Centres_docents covid_c1_m4 = new COVID_Centres_docents("Obert", data, 1, 20, 1, 0, null, 27, 2, 0, 1, 3, 0, 0);
			COVID_Centres_docents covid_c2_m4 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 4, 0, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c3_m4 = new COVID_Centres_docents("Obert", data, 1, 10, 1, 0, null, 22, 2, 0, 1, 2, 0, 0);
			COVID_Centres_docents covid_c4_m4 = new COVID_Centres_docents("Obert", data, 1, 40, 1, 0, null, 12, 2, 2, 1, 0, 0, 0);
			COVID_Centres_docents covid_c5_m4 = new COVID_Centres_docents("Obert", data, 1, 17, 1, 0, null, 3, 1, 0, 1, 1, 0, 0);
			
			COVID_Centres_docents covid_c1_m5 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 6, 1, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m5 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 23, 1, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c3_m5 = new COVID_Centres_docents("Obert", data, 0, 1, 0, 0, null, 3, 0, 0, 0, 0, 0, 0);
			
			COVID_Centres_docents covid_c1_m6 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 9, 2, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m6 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 34, 3, 0, 0, 1, 0, 0);
			COVID_Centres_docents covid_c3_m6 = new COVID_Centres_docents("Obert", data, 2, 35, 3, 0, null, 17, 1, 0, 2, 5, 1, 0);
			
			COVID_Centres_docents covid_c1_m7 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 15, 2, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m7 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 14, 3, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c3_m7 = new COVID_Centres_docents("Obert", data, 3, 85, 2, 0, null, 41, 9, 0, 3, 10, 3, 0);
			COVID_Centres_docents covid_c4_m7 = new COVID_Centres_docents("Obert", data, 2, 46, 1, 0, null, 19, 3, 0, 2, 1, 1, 0);
			COVID_Centres_docents covid_c5_m7 = new COVID_Centres_docents("Obert", data, 1, 24, 1, 0, null, 30, 1, 0, 1, 2, 0, 0);
			
			COVID_Centres_docents covid_c1_m8 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 1, 0, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m8 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 11, 1, 0, 0, 1, 0, 0);
			
			COVID_Centres_docents covid_c1_m9 = new COVID_Centres_docents("Obert", data, 1, 20, 0, 0, null, 14, 4, 0, 1, 1, 0, 0);
			COVID_Centres_docents covid_c2_m9 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 1, 0, 0, 0, 0, 0, 0);
			
			COVID_Centres_docents covid_c1_m10 = new COVID_Centres_docents("Obert", data, 0, 0, 0, 0, null, 2, 0, 0, 0, 0, 0, 0);
			COVID_Centres_docents covid_c2_m10 = new COVID_Centres_docents("Obert", data, 1, 24, 1, 0, null, 16, 2, 0, 1, 1, 0, 0);
			COVID_Centres_docents covid_c3_m10 = new COVID_Centres_docents("Obert", data, 2, 40, 4, 0, null, 7, 0, 0, 2, 2, 0, 0);
			COVID_Centres_docents covid_c4_m10 = new COVID_Centres_docents("Obert", data, 0, 29, 0, 0, null, 12, 1, 0, 0, 1, 0, 0);
			COVID_Centres_docents covid_c5_m10 = new COVID_Centres_docents("Obert", data, 2, 52, 1, 0, null, 6, 0, 0, 0, 0, 0, 0);
			
			m1.getCentres().add(c1_m1);
			m1.getCentres().add(c2_m1);
			m1.getCentres().add(c3_m1);
			m1.getCentres().add(c4_m1);
			m1.getCentres().add(c5_m1);
			
			m2.getCentres().add(c1_m2);
			m2.getCentres().add(c2_m2);
			
			m3.getCentres().add(c1_m3);
			m3.getCentres().add(c2_m3);
			m3.getCentres().add(c3_m3);
			
			m4.getCentres().add(c1_m4);
			m4.getCentres().add(c2_m4);
			m4.getCentres().add(c3_m4);
			m4.getCentres().add(c4_m4);
			m4.getCentres().add(c5_m4);
			
			m5.getCentres().add(c1_m5);
			m5.getCentres().add(c2_m5);
			m5.getCentres().add(c3_m5);
			
			m6.getCentres().add(c1_m6);
			m6.getCentres().add(c2_m6);
			m6.getCentres().add(c3_m6);
			
			m7.getCentres().add(c1_m7);
			m7.getCentres().add(c2_m7);
			m7.getCentres().add(c3_m7);
			m7.getCentres().add(c4_m7);
			m7.getCentres().add(c5_m7);
			
			m8.getCentres().add(c1_m8);
			m8.getCentres().add(c2_m8);
			
			m9.getCentres().add(c1_m9);
			m9.getCentres().add(c2_m9);
			
			m10.getCentres().add(c1_m10);
			m10.getCentres().add(c2_m10);
			m10.getCentres().add(c3_m10);
			m10.getCentres().add(c4_m10);
			m10.getCentres().add(c5_m10);
			
			c1_m1.setCodi_municipi(m1);
			c2_m1.setCodi_municipi(m1);
			c3_m1.setCodi_municipi(m1);
			c4_m1.setCodi_municipi(m1);
			c5_m1.setCodi_municipi(m1);
			
			c1_m2.setCodi_municipi(m2);
			c2_m2.setCodi_municipi(m2);
			
			c1_m3.setCodi_municipi(m3);
			c2_m3.setCodi_municipi(m3);
			c3_m3.setCodi_municipi(m3);
			
			c1_m4.setCodi_municipi(m4);
			c2_m4.setCodi_municipi(m4);
			c3_m4.setCodi_municipi(m4);
			c4_m4.setCodi_municipi(m4);
			c5_m4.setCodi_municipi(m4);
			
			c1_m5.setCodi_municipi(m5);
			c2_m5.setCodi_municipi(m5);
			c3_m5.setCodi_municipi(m5);
			
			c1_m6.setCodi_municipi(m6);
			c2_m6.setCodi_municipi(m6);
			c3_m6.setCodi_municipi(m6);
			
			c1_m7.setCodi_municipi(m7);
			c2_m7.setCodi_municipi(m7);
			c3_m7.setCodi_municipi(m7);
			c4_m7.setCodi_municipi(m7);
			c5_m7.setCodi_municipi(m7);
			
			c1_m8.setCodi_municipi(m8);
			c2_m8.setCodi_municipi(m8);
			
			c1_m9.setCodi_municipi(m9);
			c2_m9.setCodi_municipi(m9);
			
			c1_m10.setCodi_municipi(m10);
			c2_m10.setCodi_municipi(m10);
			c3_m10.setCodi_municipi(m10);
			c4_m10.setCodi_municipi(m10);
			c5_m10.setCodi_municipi(m10);
			
			covid_c1_m1.setCodi_de_centre(c1_m1);
			covid_c2_m1.setCodi_de_centre(c2_m1);
			covid_c3_m1.setCodi_de_centre(c3_m1);
			covid_c4_m1.setCodi_de_centre(c4_m1);
			covid_c5_m1.setCodi_de_centre(c5_m1);
			
			covid_c1_m2.setCodi_de_centre(c1_m2);
			covid_c2_m2.setCodi_de_centre(c2_m2);
			
			covid_c1_m3.setCodi_de_centre(c1_m3);
			covid_c2_m3.setCodi_de_centre(c2_m3);
			covid_c3_m3.setCodi_de_centre(c3_m3);
			
			covid_c1_m4.setCodi_de_centre(c1_m4);
			covid_c2_m4.setCodi_de_centre(c2_m4);
			covid_c3_m4.setCodi_de_centre(c3_m4);
			covid_c4_m4.setCodi_de_centre(c4_m4);
			covid_c5_m4.setCodi_de_centre(c5_m4);

			covid_c1_m5.setCodi_de_centre(c1_m5);
			covid_c2_m5.setCodi_de_centre(c2_m5);
			covid_c3_m5.setCodi_de_centre(c3_m5);
			
			covid_c1_m6.setCodi_de_centre(c1_m6);
			covid_c2_m6.setCodi_de_centre(c2_m6);
			covid_c3_m6.setCodi_de_centre(c3_m6);
			
			covid_c1_m7.setCodi_de_centre(c1_m7);
			covid_c2_m7.setCodi_de_centre(c2_m7);
			covid_c3_m7.setCodi_de_centre(c3_m7);
			covid_c4_m7.setCodi_de_centre(c4_m7);
			covid_c5_m7.setCodi_de_centre(c5_m7);
			
			covid_c1_m8.setCodi_de_centre(c1_m8);
			covid_c2_m8.setCodi_de_centre(c2_m8);
			
			covid_c1_m9.setCodi_de_centre(c1_m9);
			covid_c2_m9.setCodi_de_centre(c2_m9);
			
			covid_c1_m10.setCodi_de_centre(c1_m10);
			covid_c2_m10.setCodi_de_centre(c2_m10);
			covid_c3_m10.setCodi_de_centre(c3_m10);
			covid_c4_m10.setCodi_de_centre(c4_m10);
			covid_c5_m10.setCodi_de_centre(c5_m10);
			
			c1_m1.getCovid_centres().add(covid_c1_m1);
			c2_m1.getCovid_centres().add(covid_c2_m1);
			c3_m1.getCovid_centres().add(covid_c3_m1);
			c4_m1.getCovid_centres().add(covid_c4_m1);
			c5_m1.getCovid_centres().add(covid_c5_m1);
		
			c1_m2.getCovid_centres().add(covid_c1_m2);
			c2_m2.getCovid_centres().add(covid_c2_m2);
			
			c1_m3.getCovid_centres().add(covid_c1_m3);
			c2_m3.getCovid_centres().add(covid_c2_m3);
			c3_m3.getCovid_centres().add(covid_c3_m3);
			
			c1_m4.getCovid_centres().add(covid_c1_m4);
			c2_m4.getCovid_centres().add(covid_c2_m4);
			c3_m4.getCovid_centres().add(covid_c3_m4);
			c4_m4.getCovid_centres().add(covid_c4_m4);
			c5_m4.getCovid_centres().add(covid_c5_m4);
			
			c1_m5.getCovid_centres().add(covid_c1_m5);
			c2_m5.getCovid_centres().add(covid_c2_m5);
			c3_m5.getCovid_centres().add(covid_c3_m5);
			
			c1_m6.getCovid_centres().add(covid_c1_m6);
			c2_m6.getCovid_centres().add(covid_c2_m6);
			c3_m6.getCovid_centres().add(covid_c3_m6);
			
			c1_m7.getCovid_centres().add(covid_c1_m7);
			c2_m7.getCovid_centres().add(covid_c2_m7);
			c3_m7.getCovid_centres().add(covid_c3_m7);
			c4_m7.getCovid_centres().add(covid_c4_m7);
			c5_m7.getCovid_centres().add(covid_c5_m7);
	
			c1_m8.getCovid_centres().add(covid_c1_m8);
			c2_m8.getCovid_centres().add(covid_c2_m8);
			
			c1_m9.getCovid_centres().add(covid_c1_m9);
			c2_m9.getCovid_centres().add(covid_c2_m9);
			
			c1_m10.getCovid_centres().add(covid_c1_m10);
			c2_m10.getCovid_centres().add(covid_c2_m10);
			c3_m10.getCovid_centres().add(covid_c3_m10);
			c4_m10.getCovid_centres().add(covid_c4_m10);
			c5_m10.getCovid_centres().add(covid_c5_m10);
			
			session.save(m1);
			session.save(m2);
			session.save(m3);
			session.save(m4);
			session.save(m5);
			session.save(m6);
			session.save(m7);
			session.save(m8);
			session.save(m9);
			session.save(m10);
			session.save(u1);
			session.save(u2);
			session.save(u3);
			session.save(c1_m1);
			session.save(c2_m1);
			session.save(c3_m1);
			session.save(c4_m1);
			session.save(c5_m1);
			session.save(c1_m2);
			session.save(c2_m2);
			session.save(c1_m3);
			session.save(c2_m3);
			session.save(c3_m3);
			session.save(c1_m4);
			session.save(c2_m4);
			session.save(c3_m4);
			session.save(c4_m4);
			session.save(c5_m4);
			session.save(c1_m5);
			session.save(c2_m5);
			session.save(c3_m5);
			session.save(c1_m6);
			session.save(c2_m6);
			session.save(c3_m6);
			session.save(c1_m7);
			session.save(c2_m7);
			session.save(c3_m7);
			session.save(c4_m7);
			session.save(c5_m7);
			session.save(c1_m8);
			session.save(c2_m8);
			session.save(c1_m9);
			session.save(c2_m9);
			session.save(c1_m10);
			session.save(c2_m10);
			session.save(c3_m10);
			session.save(c4_m10);
			session.save(c5_m10);
			
	        //Commit the changes
	        session.getTransaction().commit();  
		} catch (HibernateException e) {
			e.printStackTrace();
			//si fallase la generacion de sesion y fuese null eso seria una excepcion que lanzaria otra excepcion encadenada.
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				//haces rollback de la transaccion. los cambios no se efectuan para TODA la transaccion
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			//finally, lo ejecuta tanto si hace el try correctamente como si hay excepcion y salta el catch
		} finally {
			if (session != null) {
			//cierra la sesion si existe
			session.close();
			}
		}	
	}
}
