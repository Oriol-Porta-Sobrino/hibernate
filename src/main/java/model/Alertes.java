package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Alertes")
public class Alertes implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int id;
	
	@Column(name = "Contingut", length = 50, nullable = false)
	private String contingut;
	
	@Column(name = "Data", nullable = false)
	@Temporal(value = TemporalType.DATE)
	private Date data;
	
	@Column(name = "Tipus_alerta", length = 50, nullable = false)
	private String tipo_alerta;
	
	@ManyToOne
	@JoinColumn(name = "Identificador_taula_estadistica")
	private Estadistiques_Municipis id_taula_estadistica;

	public Alertes() {
		super();
	}

	public Alertes(String contingut, Date data, String tipo_alerta) {
		super();
		this.contingut = contingut;
		this.data = data;
		this.tipo_alerta = tipo_alerta;
	}

	public int getId() {
		return id;
	}

	public String getContingut() {
		return contingut;
	}

	public void setContingut(String contingut) {
		this.contingut = contingut;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTipo_alerta() {
		return tipo_alerta;
	}

	public void setTipo_alerta(String tipo_alerta) {
		this.tipo_alerta = tipo_alerta;
	}

	public Estadistiques_Municipis getId_taula_estadistica() {
		return id_taula_estadistica;
	}

	public void setId_taula_estadistica(Estadistiques_Municipis id_taula_estadistica) {
		this.id_taula_estadistica = id_taula_estadistica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contingut == null) ? 0 : contingut.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + id;
		result = prime * result + ((id_taula_estadistica == null) ? 0 : id_taula_estadistica.hashCode());
		result = prime * result + ((tipo_alerta == null) ? 0 : tipo_alerta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alertes other = (Alertes) obj;
		if (contingut == null) {
			if (other.contingut != null)
				return false;
		} else if (!contingut.equals(other.contingut))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id != other.id)
			return false;
		if (id_taula_estadistica == null) {
			if (other.id_taula_estadistica != null)
				return false;
		} else if (!id_taula_estadistica.equals(other.id_taula_estadistica))
			return false;
		if (tipo_alerta == null) {
			if (other.tipo_alerta != null)
				return false;
		} else if (!tipo_alerta.equals(other.tipo_alerta))
			return false;
		return true;
	}
	
	
	
}
