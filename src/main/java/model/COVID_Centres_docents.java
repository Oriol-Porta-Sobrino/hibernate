package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "COVID_Centres_docents")
public class COVID_Centres_docents implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "Estat", length = 50, nullable = false)
	private String estat;
	
	@Column(name = "Data_del_registre", nullable = false)
	@Temporal(value = TemporalType.DATE)
	private Date data_del_registre;
	
	@Column(name = "Grups_Confin", nullable = true)
	private Integer grups_confin;
	
	@Column(name = "Alumnes_Confin", nullable = false)
	private Integer alumnes_confin;
	
	@Column(name = "Docents_Confin", nullable = false)
	private Integer docents_confin;
	
	@Column(name = "Altres_Confin", nullable = false)
	private Integer altres_confin;
	
	@Column(name = "Grups_Positiu_Acum", nullable = true)
	private Integer grups_positiu_acum;
	
	@Column(name = "Alumnes_Positiu_Acum", nullable = false)
	private Integer alumnes_positiu_acum;
	
	@Column(name = "Docents_Positiu_Acum", nullable = false)
	private Integer docents_positiu_acum;
	
	@Column(name = "Altres_Positiu_Acum", nullable = false)
	private Integer altres_positiu_acum;
	
	@Column(name = "Grups_Positiu_Vigl4")
	private Integer grups_positiu_vigl4;
	
	@Column(name = "Alumnes_Positiu_Vigl4")
	private Integer alumnes_positiu_vigl4;
	
	@Column(name = "Docents_Positiu_Vigl4")
	private Integer docents_positiu_vigl4;
	
	@Column(name = "Altres_Positius_Vigl4")
	private Integer altres_positius_vigl4;
	
	@ManyToOne
	@JoinColumn(name = "Codi_de_centre")
	private Centres_Docents codi_de_centre;

	
	public COVID_Centres_docents() {
		super();
	}

	public COVID_Centres_docents(String estat, Date data_del_registre, Integer alumnes_confin, Integer docents_confin,
			Integer altres_confin, Integer alumnes_positiu_acum, Integer docents_positiu_acum,
			Integer altres_positiu_acum) {
		super();
		this.estat = estat;
		this.data_del_registre = data_del_registre;
		this.alumnes_confin = alumnes_confin;
		this.docents_confin = docents_confin;
		this.altres_confin = altres_confin;
		this.alumnes_positiu_acum = alumnes_positiu_acum;
		this.docents_positiu_acum = docents_positiu_acum;
		this.altres_positiu_acum = altres_positiu_acum;
	}

	public COVID_Centres_docents(String estat, Date data_del_registre, Integer grups_confin, Integer alumnes_confin,
			Integer docents_confin, Integer altres_confin, Integer grups_positiu_acum, Integer alumnes_positiu_acum, Integer docents_positiu_acum,
			Integer altres_positiu_acum, Integer grups_positiu_vigl4, Integer alumnes_positiu_vigl4, Integer docents_positiu_vigl4,
			Integer altres_positius_vigl4) {
		super();
		this.estat = estat;
		this.data_del_registre = data_del_registre;
		this.grups_confin = grups_confin;
		this.alumnes_confin = alumnes_confin;
		this.docents_confin = docents_confin;
		this.altres_confin = altres_confin;
		this.grups_positiu_acum = grups_positiu_acum;
		this.alumnes_positiu_acum = alumnes_positiu_acum;
		this.docents_positiu_acum = docents_positiu_acum;
		this.altres_positiu_acum = altres_positiu_acum;
		this.grups_positiu_vigl4 = grups_positiu_vigl4;
		this.alumnes_positiu_vigl4 = alumnes_positiu_vigl4;
		this.docents_positiu_vigl4 = docents_positiu_vigl4;
		this.altres_positius_vigl4 = altres_positius_vigl4;
	}

	public int getId() {
		return id;
	}

	public String getEstat() {
		return estat;
	}

	public void setEstat(String estat) {
		this.estat = estat;
	}

	public Date getData_del_registre() {
		return data_del_registre;
	}

	public void setData_del_registre(Date data_del_registre) {
		this.data_del_registre = data_del_registre;
	}

	public int getGrup_confin() {
		return grups_confin;
	}

	public void setGrup_confin(int grup_confin) {
		this.grups_confin = grup_confin;
	}

	public int getAlumnes_confin() {
		return alumnes_confin;
	}

	public void setAlumnes_confin(int alumnes_confin) {
		this.alumnes_confin = alumnes_confin;
	}

	public int getDocents_confin() {
		return docents_confin;
	}

	public void setDocents_confin(int docents_confin) {
		this.docents_confin = docents_confin;
	}

	public int getAltres_confin() {
		return altres_confin;
	}

	public void setAltres_confin(int altres_confin) {
		this.altres_confin = altres_confin;
	}

	public int getGrups_positiu_acum() {
		return grups_positiu_acum;
	}

	public void setGrups_positiu_acum(int grups_positiu_acum) {
		this.grups_positiu_acum = grups_positiu_acum;
	}

	public int getAlumnes_positiu_acum() {
		return alumnes_positiu_acum;
	}

	public void setAlumnes_positiu_acum(int alumnes_positiu_acum) {
		this.alumnes_positiu_acum = alumnes_positiu_acum;
	}
	
	public int getDocents_positiu_acum() {
		return docents_positiu_acum;
	}

	public void setDocents_positiu_acum(int docents_positiu_acum) {
		this.docents_positiu_acum = docents_positiu_acum;
	}

	public int getAltres_positiu_acum() {
		return altres_positiu_acum;
	}

	public void setAltres_positiu_acum(int altres_positiu_acum) {
		this.altres_positiu_acum = altres_positiu_acum;
	}

	public int getGrups_positiu_vigl4() {
		return grups_positiu_vigl4;
	}

	public void setGrups_positiu_vigl4(int grups_positiu_vigl4) {
		this.grups_positiu_vigl4 = grups_positiu_vigl4;
	}

	public int getAlumnes_positiu_vigl4() {
		return alumnes_positiu_vigl4;
	}

	public void setAlumnes_positiu_vigl4(int alumnes_positiu_vigl4) {
		this.alumnes_positiu_vigl4 = alumnes_positiu_vigl4;
	}

	public int getDocents_positiu_vigl4() {
		return docents_positiu_vigl4;
	}

	public void setDocents_positiu_vigl4(int docents_positiu_vigl4) {
		this.docents_positiu_vigl4 = docents_positiu_vigl4;
	}

	public int getAltres_positius_vigl4() {
		return altres_positius_vigl4;
	}

	public void setAltres_positius_vigl4(int altres_positius_vigl4) {
		this.altres_positius_vigl4 = altres_positius_vigl4;
	}

	public Centres_Docents getCodi_de_centre() {
		return codi_de_centre;
	}

	public void setCodi_de_centre(Centres_Docents codi_de_centre) {
		this.codi_de_centre = codi_de_centre;
	}

}
