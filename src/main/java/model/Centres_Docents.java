package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Centres_Docents")
public class Centres_Docents implements Serializable {
	
	@Id
	@Column(name = "Codi_centre")
	private int codi_centre;
	
	@Column(name = "Denominaci�_completa", length = 50)
	private String denominacio_completa;
	
	@Column(name = "Adre�a", length = 50)
	private String adreca;
	
	@Column(name = "Codi_postal", length = 10)
	private String codi_postal;
	
	//@ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "centres", fetch = FetchType.EAGER)
	//private Set<Centres_Docents> centres_relacionats = new HashSet<Centres_Docents>();
	
	@ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinTable(name = "RelacioCentres", joinColumns = {@JoinColumn(name = "IdCentre")}, inverseJoinColumns = {@JoinColumn(name = "IdCentre_relacionat")})
	private Set<Centres_Docents> centres = new HashSet<Centres_Docents>();
	
	@ManyToOne
	@JoinColumn(name = "Codi_municipi")
	private Municipis codi_municipi;
	
	@OneToMany(mappedBy = "codi_de_centre", fetch = FetchType.EAGER)
	private Set<COVID_Centres_docents> covid_centres = new HashSet<COVID_Centres_docents>();

	
	public Centres_Docents() {
		super();
	}
	
	public Centres_Docents(int codi_centre, String denominacio_completa, String adreca, String codi_postal) {
		super();
		this.codi_centre = codi_centre;
		this.denominacio_completa = denominacio_completa;
		this.adreca = adreca;
		this.codi_postal = codi_postal;
	}


	public int getCodi_centre() {
		return codi_centre;
	}

	public String getDenominacio_completa() {
		return denominacio_completa;
	}

	public void setDenominacio_completa(String denominacio_completa) {
		this.denominacio_completa = denominacio_completa;
	}

	public String getAdreca() {
		return adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	public String getCodi_postal() {
		return codi_postal;
	}

	public void setCodi_postal(String codi_postal) {
		this.codi_postal = codi_postal;
	}

	/*public Set<Centres_Docents> getCentres_relacionats() {
		return centres_relacionats;
	}

	public void setCentres_relacionats(Set<Centres_Docents> centres_relacionats) {
		this.centres_relacionats = centres_relacionats;
	}*/

	public Set<Centres_Docents> getCentres() {
		return centres;
	}

	public void setCentres(Set<Centres_Docents> centres) {
		this.centres = centres;
	}

	public Municipis getCodi_municipi() {
		return codi_municipi;
	}

	public void setCodi_municipi(Municipis codi_municipi) {
		this.codi_municipi = codi_municipi;
	}

	public Set<COVID_Centres_docents> getCovid_centres() {
		return covid_centres;
	}

	public void setCovid_centres(Set<COVID_Centres_docents> covid_centres) {
		this.covid_centres = covid_centres;
	}

}
