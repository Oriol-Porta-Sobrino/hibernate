package model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "Estadistiques_Municipis")
public class Estadistiques_Municipis implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int id;
	
	@Column(name = "incidencia_acumulada", nullable = true)
	private int incidencia_acumulada;
	
	@Column(name = "Data_de_generacio", nullable = false)
	@Temporal(value = TemporalType.DATE)
	private Date data_generacio;
	
	@Type(type="numeric_boolean")
	@Column(name = "Generar_alerta", nullable = true)
	private boolean generar_alerta;
	
	@Type(type="numeric_boolean")
	@Column(name = "Alerta_creada", nullable = true)
	private boolean alerta_creada;
	
	@Column(name = "Risc_rebrot", nullable = true)
	private int risc_rebrot;
	
	@Column(name = "Velocitat_de_propagacio", nullable = true)
	private int velocitat_propagacio;
	
	@Column(name = "Nous_positius", nullable = true)
	private int nous_positius;
	
	@Column(name = "Acumulat_dies", nullable = true)
	private int acumulat_dies;
	
	@Column(name = "Positius_N1", nullable = true)
	private int positius_n1;
	
	@Column(name = "Positius_N2", nullable = true)
	private int positius_n2;
	
	@Column(name = "Positius_N5", nullable = true)
	private int positius_n5;
	
	@Column(name = "Positius_N6", nullable = true)
	private int positius_n6;
	
	@Column(name = "Positius_N7", nullable = true)
	private int positius_n7;
	
	@Type(type="numeric_boolean")
	@Column(name = "Realitzada", nullable = true)
	private boolean realizada;
	
	@ManyToOne
	@JoinColumn(name = "Codi_municipi")
	private Municipis codi_municipi;
	
	@OneToMany(mappedBy = "id_taula_estadistica", fetch = FetchType.EAGER)
	private Set<Alertes> alertes = new HashSet<Alertes>();
	
	@ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinTable(name = "EstadistiquesUsuari", joinColumns = {@JoinColumn(name = "IdEstadistiques_municipi")}, inverseJoinColumns = {@JoinColumn(name = "IdUsuari")})
	private Set<Usuari> usuaris = new HashSet<Usuari>();
	
	public Estadistiques_Municipis() {
		super();
	}
	
	public Estadistiques_Municipis(int incidencia_acumulada, Date data_generacio, boolean generar_alerta,
			boolean alerta_creada, int risc_rebrot, int velocitat_propagacio, int nous_positius, int acumulat_dies,
			int positius_n1, int positius_n2, int positius_n5, int positius_n6, int positius_n7, boolean realizada) {
		super();
		this.incidencia_acumulada = incidencia_acumulada;
		this.data_generacio = data_generacio;
		this.generar_alerta = generar_alerta;
		this.alerta_creada = alerta_creada;
		this.risc_rebrot = risc_rebrot;
		this.velocitat_propagacio = velocitat_propagacio;
		this.nous_positius = nous_positius;
		this.acumulat_dies = acumulat_dies;
		this.positius_n1 = positius_n1;
		this.positius_n2 = positius_n2;
		this.positius_n5 = positius_n5;
		this.positius_n6 = positius_n6;
		this.positius_n7 = positius_n7;
		this.realizada = realizada;
	}

	public int getId() {
		return id;
	}

	public int getIncidencia_acumulada() {
		return incidencia_acumulada;
	}

	public void setIncidencia_acumulada(int incidencia_acumulada) {
		this.incidencia_acumulada = incidencia_acumulada;
	}

	public Date getData_generacio() {
		return data_generacio;
	}

	public void setData_generacio(Date data_generacio) {
		this.data_generacio = data_generacio;
	}

	public boolean isGenerar_alerta() {
		return generar_alerta;
	}

	public void setGenerar_alerta(boolean generar_alerta) {
		this.generar_alerta = generar_alerta;
	}

	public boolean isAlerta_creada() {
		return alerta_creada;
	}

	public void setAlerta_creada(boolean alerta_creada) {
		this.alerta_creada = alerta_creada;
	}

	public int getRisc_rebrot() {
		return risc_rebrot;
	}

	public void setRisc_rebrot(int risc_rebrot) {
		this.risc_rebrot = risc_rebrot;
	}

	public int getVelocitat_propagacio() {
		return velocitat_propagacio;
	}

	public void setVelocitat_propagacio(int velocitat_propagacio) {
		this.velocitat_propagacio = velocitat_propagacio;
	}

	public int getNous_positius() {
		return nous_positius;
	}

	public void setNous_positius(int nous_positius) {
		this.nous_positius = nous_positius;
	}

	public int getAcumulat_dies() {
		return acumulat_dies;
	}

	public void setAcumulat_dies(int acumulat_dies) {
		this.acumulat_dies = acumulat_dies;
	}

	public int getPositius_n1() {
		return positius_n1;
	}

	public void setPositius_n1(int positius_n1) {
		this.positius_n1 = positius_n1;
	}

	public int getPositius_n2() {
		return positius_n2;
	}

	public void setPositius_n2(int positius_n2) {
		this.positius_n2 = positius_n2;
	}

	public int getPositius_n5() {
		return positius_n5;
	}

	public void setPositius_n5(int positius_n5) {
		this.positius_n5 = positius_n5;
	}

	public int getPositius_n6() {
		return positius_n6;
	}

	public void setPositius_n6(int positius_n6) {
		this.positius_n6 = positius_n6;
	}

	public int getPositius_n7() {
		return positius_n7;
	}

	public void setPositius_n7(int positius_n7) {
		this.positius_n7 = positius_n7;
	}

	public boolean isRealizada() {
		return realizada;
	}

	public void setRealizada(boolean realizada) {
		this.realizada = realizada;
	}

	public Municipis getCodi_municipi() {
		return codi_municipi;
	}

	public void setCodi_municipi(Municipis codi_municipi) {
		this.codi_municipi = codi_municipi;
	}

	public Set<Alertes> getAlertes() {
		return alertes;
	}

	public void setAlertes(Set<Alertes> alertes) {
		this.alertes = alertes;
	}

	public Set<Usuari> getUsuaris() {
		return usuaris;
	}

	public void setUsuaris(Set<Usuari> usuaris) {
		this.usuaris = usuaris;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + acumulat_dies;
		result = prime * result + (alerta_creada ? 1231 : 1237);
		result = prime * result + ((alertes == null) ? 0 : alertes.hashCode());
		result = prime * result + ((codi_municipi == null) ? 0 : codi_municipi.hashCode());
		result = prime * result + ((data_generacio == null) ? 0 : data_generacio.hashCode());
		result = prime * result + (generar_alerta ? 1231 : 1237);
		result = prime * result + id;
		result = prime * result + incidencia_acumulada;
		result = prime * result + nous_positius;
		result = prime * result + positius_n1;
		result = prime * result + positius_n2;
		result = prime * result + positius_n5;
		result = prime * result + positius_n6;
		result = prime * result + positius_n7;
		result = prime * result + (realizada ? 1231 : 1237);
		result = prime * result + risc_rebrot;
		result = prime * result + ((usuaris == null) ? 0 : usuaris.hashCode());
		result = prime * result + velocitat_propagacio;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estadistiques_Municipis other = (Estadistiques_Municipis) obj;
		if (acumulat_dies != other.acumulat_dies)
			return false;
		if (alerta_creada != other.alerta_creada)
			return false;
		if (alertes == null) {
			if (other.alertes != null)
				return false;
		} else if (!alertes.equals(other.alertes))
			return false;
		if (codi_municipi == null) {
			if (other.codi_municipi != null)
				return false;
		} else if (!codi_municipi.equals(other.codi_municipi))
			return false;
		if (data_generacio == null) {
			if (other.data_generacio != null)
				return false;
		} else if (!data_generacio.equals(other.data_generacio))
			return false;
		if (generar_alerta != other.generar_alerta)
			return false;
		if (id != other.id)
			return false;
		if (incidencia_acumulada != other.incidencia_acumulada)
			return false;
		if (nous_positius != other.nous_positius)
			return false;
		if (positius_n1 != other.positius_n1)
			return false;
		if (positius_n2 != other.positius_n2)
			return false;
		if (positius_n5 != other.positius_n5)
			return false;
		if (positius_n6 != other.positius_n6)
			return false;
		if (positius_n7 != other.positius_n7)
			return false;
		if (realizada != other.realizada)
			return false;
		if (risc_rebrot != other.risc_rebrot)
			return false;
		if (usuaris == null) {
			if (other.usuaris != null)
				return false;
		} else if (!usuaris.equals(other.usuaris))
			return false;
		if (velocitat_propagacio != other.velocitat_propagacio)
			return false;
		return true;
	}
	
}
