package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Municipis")
public class Municipis implements Serializable {

	@Id
	@Column(name = "Codi_municipi")
	private int codi_municipi;
	
	@Column(name = "Nom_municipi", length = 50, nullable = false)
	private String nom_municipi;
	
	@Column(name = "Codi_comarca", nullable = false)
	private int codi_comarca;
	
	@Column(name = "Nom_comarca", length = 50, nullable = false)
	private String nom_comarca;
	
	@Column(name = "Poblacio", nullable = false)
	private int poblacio;
	
	@OneToMany(mappedBy="codi_municipi", fetch = FetchType.EAGER)
    Set<Centres_Docents> centres = new HashSet<Centres_Docents>();
    
    @OneToMany(mappedBy = "codi_municipi", fetch = FetchType.EAGER)
    Set<Estadistiques_Municipis> estadistiques = new HashSet<Estadistiques_Municipis>();

	public Municipis() {
		super();
	}

	public Municipis(int codi_municipi, String nom_municipi, int codi_comarca, String nom_comarca, int poblacio) {
		super();
		this.codi_municipi = codi_municipi;
		this.nom_municipi = nom_municipi;
		this.codi_comarca = codi_comarca;
		this.nom_comarca = nom_comarca;
		this.poblacio = poblacio;
	}

	public int getCodi_municipi() {
		return codi_municipi;
	}

	public void setCodi_municipi(int codi_municipi) {
		this.codi_municipi = codi_municipi;
	}

	public String getNom_municipi() {
		return nom_municipi;
	}

	public void setNom_municipi(String nom_municipi) {
		this.nom_municipi = nom_municipi;
	}

	public int getCodi_comarca() {
		return codi_comarca;
	}

	public void setCodi_comarca(int codi_comarca) {
		this.codi_comarca = codi_comarca;
	}

	public String getNom_comarca() {
		return nom_comarca;
	}

	public void setNom_comarca(String nom_comarca) {
		this.nom_comarca = nom_comarca;
	}

	public int getPoblacio() {
		return poblacio;
	}

	public void setPoblacio(int poblacio) {
		this.poblacio = poblacio;
	}

	public Set<Centres_Docents> getCentres() {
		return centres;
	}

	public void setCentres(Set<Centres_Docents> centres) {
		this.centres = centres;
	}
	
	public Set<Estadistiques_Municipis> getEstadistiques() {
		return estadistiques;
	}

	public void setEstadistiques(Set<Estadistiques_Municipis> estadistiques) {
		this.estadistiques = estadistiques;
	}
	
}
