package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
//Tabla, es el elemento raiz
@Table(name="Usuari")
public class Usuari implements Serializable {

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column(name="Nom", length=50, nullable = false)
    private String nom;
    
    @Column(name="Cognom1", length=50, nullable = false)
    private String cog1;
    
    @Column(name="Cognom2", length=50, nullable = false)    
    private String cog2;

	@ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "usuaris", fetch = FetchType.EAGER)
	private Set<Estadistiques_Municipis> estadistiques = new HashSet<Estadistiques_Municipis>();
	
	public Usuari() {
		super();
	}

	public Usuari(String nom, String cog1, String cog2) {
		super();
		this.nom = nom;
		this.cog1 = cog1;
		this.cog2 = cog2;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCog1() {
		return cog1;
	}

	public void setCog1(String cog1) {
		this.cog1 = cog1;
	}

	public String getCog2() {
		return cog2;
	}

	public void setCog2(String cog2) {
		this.cog2 = cog2;
	}

	public Set<Estadistiques_Municipis> getEstadistiques() {
		return estadistiques;
	}

	public void setEstadistiques(Set<Estadistiques_Municipis> estadistiques) {
		this.estadistiques = estadistiques;
	}
	
}
